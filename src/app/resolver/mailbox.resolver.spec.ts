import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { MailBoxListModel } from '../models/MailBoxModel';
import { UserDetailCookieModel } from '../models/userDetailCookieModel';
import { GlobalService } from '../services/global.service';
import { MailboxService } from '../services/mailbox.service';
import { UserDetailService } from '../services/userDetail.service';
import { MailboxResolver } from './mailbox.resolver';

describe('MailboxResolver', () => {
  let resolver: MailboxResolver;
  let mailBoxServiceStub: Partial<MailboxService>;
  let userDetailServiceStub: Partial<UserDetailService>;
  const mockGlobalService: GlobalService = new GlobalService();
  const mockMailBoxesList: MailBoxListModel = {
    "mailboxes":
      [{
        "mailbox_id": 503,
        "mailbox_name": "demo-fwd-planning@gmail.com"
      },
      {
        "mailbox_id": 504,
        "mailbox_name": "demo2@gmail.com"
      }
      ],
    "user_id": "107"
  };


  beforeEach(() => {

    mailBoxServiceStub = {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      fetchAllMailBoxesWrtToUser(urlQueryStr: string): Observable<MailBoxListModel> {
        return of(mockMailBoxesList);
      }
    };

    userDetailServiceStub = {
      getUserDetailFromCookie(): UserDetailCookieModel {
        return {
          "aud": "",
          "iss": "",
          "iat": "",
          "nbf": "",
          "exp": "",
          "aio": "",
          "name": "Ratikanta Rout",
          "nonce": "",
          "oid": "",
          "preferred_username": "ratikanta.rout.gmail@.com",
          "uname": "Ratikanta Rout",
          "rh": "",
          "sub": "",
          "tid": "",
          "uti": "",
          "ver": "",
          "uid": 107

        };
      }
    };

    spyOn(mockGlobalService, 'getQueryString').and.callThrough();

    TestBed.configureTestingModule({
      providers: [
        {
          provide: MailboxService,
          useValue: mailBoxServiceStub
        },
        {
          provide: UserDetailService,
          useValue: userDetailServiceStub
        },
        {
          provide: GlobalService
        }
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    });
    resolver = TestBed.inject(MailboxResolver);
  });

  it('should be able to create resolver', () => {
    expect(resolver).toBeTruthy();
  });

  it('should be able to get all mailboxes', () => {
    resolver.resolve().subscribe(
      data => {
        expect(data).toEqual(mockMailBoxesList);
      }
    );
  });
});
