import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MailBoxListModel } from '../models/MailBoxModel';
import { GlobalService } from '../services/global.service';
import { MailboxService } from '../services/mailbox.service';
import { UserDetailService } from '../services/userDetail.service';

@Injectable({
  providedIn: 'root'
})
export class MailboxResolver implements Resolve<MailBoxListModel> {
  constructor(private _mailboxService: MailboxService,
    private _userDetailService: UserDetailService,
    private _globalService: GlobalService) {

  }
  resolve(): Observable<MailBoxListModel> {
    const userDetail = this._userDetailService.getUserDetailFromCookie()
    const queryString = this._globalService.getQueryString({
      "email": userDetail.preferred_username
    });
    return this._mailboxService.fetchAllMailBoxesWrtToUser(queryString).pipe(
      map(data => data));
  }

}

