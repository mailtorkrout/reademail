export interface ReviewSubmitModel {
    uuid: string,
    reviewed_by: number,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    review_date: any,
    reviewed_decision_id: string,
    review_comments: string
}

export interface ReviewSubmitResponseModel {
    msg: string
}
