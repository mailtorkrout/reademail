import { IFeed } from "./feedModel";

export interface FeedWithPaginationModel {

    mails: IFeed[],
    max_records: number
    total_pages: number
    total_records: number

}