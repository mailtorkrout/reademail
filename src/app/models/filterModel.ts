export interface FilterModel {
    relevancy: string,
    start_date: string,
    end_date: string,
    originator: string,
    page: string,
    originator_id: string,
    reviewed_decision_id: string,
    shared_mailbox_id:string
}