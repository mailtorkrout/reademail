export interface MailBoxModel {
    mailbox_id: number,
    mailbox_name: string
}

export interface MailBoxListModel {
    mailboxes: MailBoxModel[],
    user_id: string
}