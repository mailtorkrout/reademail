export interface IWeatherForecast {
date: Date;
summary: string;
temperatureC: number;
temperatureF: number;
}

export interface IProcessedEmail{
    fileName: string;
    sender: string;
    msgSubject: string;
    msgDate: Date;
    createdOn: Date;
    msgBody: string;
    translatedText: string;
    msgProcessedBody: string;
    score: number;
    accepted: boolean;
    rejected: boolean;
}
