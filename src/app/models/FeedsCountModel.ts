export interface FeedsCountModel {
    acccepted: number,
    irrelevent: number,
    pending: number,
    rejected: number,
    reviewed: number,
    relevent: number
}