export interface UserDetailCookieModel {
    aud: string,
    iss: string,
    iat: string,
    nbf: string,
    exp: string,
    aio: string,
    name: string,
    nonce: string,
    oid: string,
    preferred_username: string,
    uname: string,
    rh: string,
    sub: string,
    tid: string,
    uti: string,
    ver: string,
    uid: number
}