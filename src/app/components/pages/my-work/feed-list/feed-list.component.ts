/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { Component, OnInit } from '@angular/core';
import { FeedsService } from '../../../../services/feeds.service';
import { FilterService } from '../../../../services/feeds.filter.service';
import { IFeed } from '../../../../models/feedModel';
import { DEFAULT_PAGE } from 'src/app/app.config';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-feed-list',
  templateUrl: './feed-list.component.html',
  styleUrls: ['./feed-list.component.sass']
})

export class FeedListComponent implements OnInit {

  public activePage = 1;

  public feedList: IFeed[] = [];

  public selectedFeedNumber = 0;

  constructor(private _feedsService: FeedsService,
    private _filterService: FilterService,
    private _globalService: GlobalService) {
  }

  /**
   * After getting feeds with filter
   */
  ngOnInit(): void {
    this._feedsService.getFeedsWithFilter()
      .subscribe(data => {
        this.feedList = data;
        this.selectedFeedNumber = 0;
        const selectedPage = this._filterService.getSelectedPage();
        if (selectedPage === DEFAULT_PAGE)
          if (data.length) {
            this.getFeedDetail(this.selectedFeedNumber, data[0]);
          } else {
            this.getFeedDetail(this.selectedFeedNumber, {} as IFeed);
          }
      });
    this.selectedFeedNumber = 0;
  }

  /**
   * On scroll of feed list present in left bar, this function will called.
   * @param event event
   */
  onScroll(event: any): void {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const feedListElem: HTMLElement = document.getElementById("feedList")!;
    console.log("CLient Height :-"+feedListElem.clientHeight);
    console.log("Scrolltop :-"+feedListElem.scrollTop);
    console.log("add :-"+(feedListElem.scrollTop + feedListElem.clientHeight));
    console.log("ScrollHeight :-"+feedListElem.scrollHeight);
    if ((feedListElem.scrollHeight)-(feedListElem.scrollTop + feedListElem.clientHeight) <= 50 ) {
      this.loadMore();
    }
  }

  /**
   * On Load more we are triggering for next page.
   */
  private loadMore() {
    console.log("load");
    if (parseInt(this._filterService.getSelectedPage()) <= (this._feedsService.getTotalPages() - 1)) {
      this._filterService.sendSelectedPageBSubject(
        parseInt(this._filterService.getSelectedPage()) + 1);
    }
  }

  /**
   * 
   * Get feed details and write to iframe and updated with selected feed number.
   * 
   * @param selectedFeedNumber selected Feed Number
   * @param feedDetail feed Detail
   */
  getFeedDetail(selectedFeedNumber: number, feedDetail: IFeed): void {
    console.log("feedDetail", feedDetail);
    this._feedsService.sendFeedDetail(feedDetail);
    this._globalService.writetoIframe(feedDetail);
    this.selectedFeedNumber = selectedFeedNumber;
  }

}
