/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FeedsService } from 'src/app/services/feeds.service';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { IFeed } from 'src/app/models/feedModel';
import { GlobalService } from 'src/app/services/global.service';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FeedListComponent } from './feed-list.component';
import { FilterService } from 'src/app/services/feeds.filter.service';
import { event } from 'jquery';
import { BLANK } from 'src/app/app.config';

describe('FeedListComponent', () => {
    let component: FeedListComponent;
    let fixture: ComponentFixture<FeedListComponent>;
    let FeedsServiceStub: Partial<FeedsService>;
    let feedService: FeedsService;
    let mockGlobalService: GlobalService;
    let filterService: FilterService;
    let httpClientSpy: { get: jasmine.Spy };
    const iFeed: IFeed = {
        "batch_id": 1376,
        "discovered_dates": ["year"],
        "discovered_keywords": ['year'],
        "future_date_score": 1.00,
        "is_actual_html": true,
        "keyword_score": 0.00,
        "listed_domain": false,
        "msg_body": "Hello msg_body",
        "msg_body_HTML": "Hello msg_body_HTML year",
        "msg_body_translated": "",
        "msg_from": "sadashiv.borkar.gmail@.com",
        "msg_id": "\r\n <AS8PR01MB7656296C11A7083D89D86E03A71B9@AS8PR01MB7656.eurprd01.prod.exchangelabs.com>",
        "msg_received_dt": new Date(),
        "msg_subject": "Hello msg_subject",
        "msg_to": [
            "sadashiv.borkar.gmail@.com"
        ],
        "originator_id": 1,
        "relevancy": true,
        "reviewed_date": new Date(),
        "reviewed_decision_id": 30,
        "reviewedby_user_id": 15,
        "search_score": 0.00,
        "shared_mailbox_id": 503,
        "total_score": 1.00,
        "uuid": "fa9af88e-e492-11eb-a44a-afa41d49256a",
        "reviewed_by_username": "Ratikanta Rout",
        "review_comments": BLANK,
        "msg_language": "en"
    }

    beforeEach(async () => {
        httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post']);
        feedService = new FeedsService(httpClientSpy as any, mockGlobalService);
        filterService = new FilterService(feedService, mockGlobalService);

        FeedsServiceStub = {
            getFeedsWithFilter() {
                return of([iFeed])
            },
            sendFeedDetail(feedDetail: IFeed) {
                feedService.sendFeedDetail(feedDetail);
            },
            getTotalPages() {
                return feedService.getTotalPages();
            }
        };

        await TestBed.configureTestingModule({
            declarations: [FeedListComponent],
            providers: [
                {
                    provide: FeedsService,
                    useValue: FeedsServiceStub
                },
                {
                    provide: FilterService
                },
                {
                    provide: GlobalService
                }
            ],
            imports: [RouterTestingModule],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(FeedListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should be able to create feed list component', () => {
        expect(component).toBeTruthy();
    });

    it('test on scroll', () => {
        spyOn(filterService, 'sendSelectedPageBSubject').and.callThrough();
        component.onScroll(event);
        expect(component).toBeTruthy();
    });

    it('load more check', () => {
        spyOn(filterService, 'sendSelectedPageBSubject').and.callThrough();
        filterService.setSelectedPage(1);
        feedService.setTotalPages(4);
        component['loadMore']();
        expect(component).toBeTruthy();
    });

});
