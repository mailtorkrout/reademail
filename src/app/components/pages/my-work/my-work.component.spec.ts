/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { ComponentFixture, fakeAsync, TestBed, tick, } from '@angular/core/testing';
import { FeedsService } from 'src/app/services/feeds.service';
import { MyWorkComponent } from './my-work.component';
import { RouterTestingModule } from '@angular/router/testing';
import { NotifierService } from 'angular-notifier';
import { Observable, of } from 'rxjs';
import { FeedsCountModel } from 'src/app/models/FeedsCountModel';
import { IFeed } from 'src/app/models/feedModel';
import { GlobalService } from 'src/app/services/global.service';
import { ReviewSubmitModel } from 'src/app/models/ReviewSubmitModel';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('MyWorkComponent', () => {
    let component: MyWorkComponent;
    let fixture: ComponentFixture<MyWorkComponent>;
    let FeedsServiceStub: Partial<FeedsService>;
    let feedService: FeedsService;
    let mockGlobalService: GlobalService;
    let httpClientSpy: { get: jasmine.Spy };
    const iFeed: IFeed = {
        "batch_id": 1376,
        "discovered_dates": ["year"],
        "discovered_keywords": ['year'],
        "future_date_score": 1.00,
        "is_actual_html": true,
        "keyword_score": 0.00,
        "listed_domain": false,
        "msg_body": "Hello msg_body",
        "msg_body_HTML": "Hello msg_body_HTML year",
        "msg_body_translated": "",
        "msg_from": "sadashiv.borkar.gmail@.com",
        "msg_id": "\r\n <AS8PR01MB7656296C11A7083D89D86E03A71B9@AS8PR01MB7656.eurprd01.prod.exchangelabs.com>",
        "msg_received_dt": new Date(),
        "msg_subject": "Hello msg_subject",
        "msg_to": [
            "sadashiv.borkar.gmail@.com"
        ],
        "originator_id": 1,
        "relevancy": true,
        "reviewed_date": new Date(),
        "reviewed_decision_id": 30,
        "reviewedby_user_id": 15,
        "search_score": 0.00,
        "shared_mailbox_id": 503,
        "total_score": 1.00,
        "uuid": "fa9af88e-e492-11eb-a44a-afa41d49256a",
        "reviewed_by_username": "Ratikanta Rout",
        "review_comments":"",
        "msg_language": "en"
    }
    let mockFeedCount: FeedsCountModel = {
        "acccepted": 9,
        "irrelevent": 10,
        "pending": 20,
        "rejected": 10,
        "reviewed": 1,
        "relevent": 10
    }

    afterEach(async () => {
        mockFeedCount = {
            "acccepted": 9,
            "irrelevent": 10,
            "pending": 20,
            "rejected": 10,
            "reviewed": 1,
            "relevent": 10
        }
    })

    beforeEach(async () => {
        const notifierServiceSpy = jasmine.createSpyObj('NotifierService', ['show']);
        httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post']);
        feedService = new FeedsService(httpClientSpy as any, mockGlobalService);


        spyOn(feedService, 'removeFeedsByUid').and.callThrough();

        FeedsServiceStub = {
            getFeedDetail(): Observable<IFeed> {
                const mockIFeed: IFeed = iFeed;
                return of(mockIFeed);
            },
            getFeedsCount(): FeedsCountModel {
                return mockFeedCount;
            },
            setFeedCount(data: FeedsCountModel) {
                mockFeedCount = data;
            },
            submitReview(_data): Observable<ReviewSubmitModel> {
                return of({
                    "uuid": "fa9af88e-e492-11eb-a44a-afa41d49256a",
                    "reviewed_by": 15,
                    "review_date": "",
                    "review_comments":"",
                    "reviewed_decision_id": ""
                })
            },
            removeFeedsByUid(data) {
                feedService.removeFeedsByUid(data);
            },

        };



        await TestBed.configureTestingModule({
            declarations: [MyWorkComponent],
            providers: [
                {
                    provide: FeedsService,
                    useValue: FeedsServiceStub
                },
                {
                    provide: NotifierService,
                    useValue: notifierServiceSpy
                }

            ],
            imports: [RouterTestingModule],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(MyWorkComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create my work component', () => {
        expect(component).toBeTruthy();
    });

    it('should get set Selected Status Button', fakeAsync(() => {
        fixture.whenRenderingDone().then(() => {
            fixture.detectChanges();
            component.setSelectedStatusButton("Pending Review");
            fixture.detectChanges();
            expect(component.selectedStatusButton).toBe("Pending Review");
        });
    }));

    xit('check if submit review success', fakeAsync(() => {

        // fixture.whenRenderingDone().then(() => {
        //     fixture.detectChanges();
        //     spyOn(component, 'submitReview');
        //     const button = fixture.debugElement.nativeElement.querySelector('.acceptandreject btn btn-outline-success');
        //     button?.click();
        //     fixture.whenStable().then(() => {
        //         fixture.detectChanges();
        //         component.submitReview("10", iFeed);
        //         fixture.detectChanges();
        //     });
        // })
    }));


    // it('check if submit review success with pending, review count change', fakeAsync(() => {
    //     tick();
    //     component.submitReview("10", iFeed);
    //     tick();
    //     expect(mockFeedCount.reviewed).toBe(2);
    //     expect(mockFeedCount.pending).toBe(19);

    //     tick();
    //     component.submitReview("20", iFeed);
    //     tick();
    //     expect(mockFeedCount.reviewed).toBe(3);
    //     expect(mockFeedCount.pending).toBe(18);
    // }));

    it('highlight KeyWords', fakeAsync(() => {
        tick();
        component.highlightKeyWords(iFeed);
        tick();
        expect(component.is_keywords_selected).toBe(true);
    }));

    it('highlight Future Date', fakeAsync(() => {
        tick();
        component.highlightFutureDate(iFeed);
        tick();
        expect(component.is_future_date_selected).toBe(true);
    }));

    it('highlight Email and Domain', fakeAsync(() => {
        tick();
        component.highlightEmailNDomain(iFeed);
        tick();
        expect(component.is_email_domain_selected).toBe(true);
    }));

});
