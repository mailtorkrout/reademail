/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FeedsService } from 'src/app/services/feeds.service';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { FeedsCountModel } from 'src/app/models/FeedsCountModel';
import { IFeed } from 'src/app/models/feedModel';
import { GlobalService } from 'src/app/services/global.service';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FilterService } from 'src/app/services/feeds.filter.service';
import { BLANK } from 'src/app/app.config';
import { FormsModule } from '@angular/forms';
import { CountFeedsComponent } from './count-feeds.component';
import { DaterangepickerConfig } from "ng2-daterangepicker";
import { MailboxService } from 'src/app/services/mailbox.service';
import { UserDetailService } from 'src/app/services/userDetail.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import * as moment from 'moment';
import { event } from 'jquery';

describe('CountFeedsComponent', () => {
    let component: CountFeedsComponent;
    let fixture: ComponentFixture<CountFeedsComponent>;
    let FeedsServiceStub: Partial<FeedsService>;
    let feedService: FeedsService;
    let mailBoxService: MailboxService;
    let filterService: FilterService;
    let mockGlobalService: GlobalService;
    let httpClientSpy: { get: jasmine.Spy };
    let userDetailService: UserDetailService;
    let mockFeedDetail: IFeed;

    let mockFeedCount: FeedsCountModel = {
        "acccepted": 9,
        "irrelevent": 10,
        "pending": 20,
        "rejected": 10,
        "reviewed": 1,
        "relevent": 10
    }

    afterEach(async () => {
        mockFeedCount = {
            "acccepted": 9,
            "irrelevent": 10,
            "pending": 20,
            "rejected": 10,
            "reviewed": 1,
            "relevent": 10
        }
    })

    beforeEach(async () => {
        httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post']);
        mockGlobalService = new GlobalService();
        feedService = new FeedsService(httpClientSpy as any, mockGlobalService);
        filterService = new FilterService(feedService, mockGlobalService);
        userDetailService = new UserDetailService();
        mailBoxService = new MailboxService(userDetailService, httpClientSpy as any)

        mockFeedDetail = {
            "batch_id": 1038,
            "discovered_dates": ["year"],
            "discovered_keywords": ['year'],
            "future_date_score": 1.00,
            "is_actual_html": true,
            "keyword_score": 1.52,
            "listed_domain": false,
            "msg_body": "test message body",
            "msg_body_HTML": "Test message HTML",
            "msg_body_translated": BLANK,
            "msg_from": "donotreply@wordpress.com",
            "msg_id": "<59947194.47732.0@wordpress.com>",
            "msg_received_dt": new Date(),
            "msg_subject": "[New post] Critical Missing Person Richard Green",
            "msg_to": [
                "aptexas@ap.org"
            ],
            "originator_id": 1,
            "relevancy": true,
            "reviewed_date": new Date(),
            "reviewed_decision_id": 30,
            "reviewedby_user_id": 1,
            "search_score": 3.21,
            "shared_mailbox_id": 504,
            "total_score": 4.21,
            "uuid": "71734d75-e87b-11eb-a02c-33d8c57487e9",
            "reviewed_by_username": BLANK,
            "review_comments": BLANK,
            "msg_language": "en"
        };

        FeedsServiceStub = {
            setFeedCount(data: FeedsCountModel) {
                mockFeedCount = data;
            },
            fetchAllFeedsWithFilter(url: string) {
                return of({
                    "mails": [
                        mockFeedDetail
                    ],
                    "max_records": 10,
                    "total_pages": 1,
                    "total_records": 1
                })
            },
            sendTotalRecordsBSubject(data: number) {
                feedService.sendTotalRecordsBSubject(data);
            },
            setTotalPages(data: number) {
                feedService.setTotalPages(data);
            },
            setFeeds(feeds: IFeed[]) {
                feedService.setFeeds(feeds);
            },
            sendFeeds() {
                feedService.sendFeeds();
            },
            getFeedsCountSubject() {
                spyOn(feedService, 'getFeedsCountSubject').and.returnValue(
                    of(mockFeedCount)
                );
                return feedService.getFeedsCountSubject();
            },
            fetchAllFeedCount(mailBoxId: number, start_date?: string, end_date?: string) {
                return of(mockFeedCount)
            }
        };
        await TestBed.configureTestingModule({
            declarations: [CountFeedsComponent],
            providers: [
                DaterangepickerConfig,
                {
                    provide: FeedsService,
                    useValue: FeedsServiceStub
                },
                { provide: FilterService },
                { provide: MailboxService },
                { provide: GlobalService }
            ],
            imports: [RouterTestingModule, FormsModule, HttpClientTestingModule],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(CountFeedsComponent);
        component = fixture.componentInstance;
        component.updateSettings();
        fixture.detectChanges();

    });

    it('should create count feed component', () => {
        expect(component).toBeTruthy();
    });

    it('test get Pending Feeds', () => {
        component.selectedStatusButton = component.statusButtons.pending_review.label;
        component.getPendingFeeds();
        filterService.getSelectedPageAsObservable().subscribe(
            data => { expect(data).toBe("1"); }
        )
    });

    it('test get Reviewed Feeds', () => {
        component.selectedStatusButton = component.statusButtons.reviewed.label;
        component.getReviewedFeeds();
        filterService.getSelectedPageAsObservable().subscribe(
            data => { expect(data).toBe("1"); }
        )
    });

    it('test get relevant feeds', () => {
        component.selectedStatusButton = component.statusButtons.relevant.label;
        component.getRelevantFeeds();
        filterService.getSelectedPageAsObservable().subscribe(
            data => { expect(data).toBe("1"); }
        )
    });


    it('test get irr relevant feeds', () => {
        component.selectedStatusButton = component.statusButtons.irrelevant.label;
        component.getIrrelevantFeeds();
        filterService.getSelectedPageAsObservable().subscribe(
            data => { expect(data).toBe("1"); }
        )
    });

    it('selected date check', () => {
        component.selectedDate(
            {
                start: moment().subtract(-1, 'day'),
                end: moment().subtract('day'),
                label: "Today"
            }
            , {
                start: moment().subtract(-1, 'day'),
                end: moment().subtract('day')
            })
        expect(component.dateSelectedlabel).toBe("Today");
    });

    // it('on Mail Box Change Get Feeds Count test', () => {
    //     component.onMailBoxChangeGetFeedsCount();
    //     filterService.getSlectedOriginatorId().subscribe(
    //         data => { expect(data).toBe(BLANK); }
    //     )
    // });
});
