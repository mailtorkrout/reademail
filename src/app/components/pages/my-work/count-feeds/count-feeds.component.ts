/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import * as moment from 'moment';
import { DaterangepickerConfig } from "ng2-daterangepicker";
import { FeedsService } from '../../../../services/feeds.service';
import { FilterService } from '../../../../services/feeds.filter.service';
import { GlobalService } from '../../../../services/global.service';
import { FeedsCountModel } from '../../../../models/FeedsCountModel';
import { DEFAULT_PAGE, BLANK } from 'src/app/app.config';

@Component({
  selector: 'app-count-feeds',
  templateUrl: './count-feeds.component.html',
  styleUrls: ['./count-feeds.component.sass']
})

/**
 * Count Feeds Component will used as count of all fields.
 */
export class CountFeedsComponent implements OnInit {

  private defaultOriginatorId = BLANK;

  @Output() selectedStatusBtnEvent = new EventEmitter<string>();

  public statusButtons = {
    "pending_review": {
      label: "Pending Review",
      id: 0,
      count: 0,
    },
    "reviewed": {
      label: "Reviewed",
      id: 1,
      count: 0,
    },
    "relevant": {
      label: "Relevant",
      id: 2,
      count: 0
    },
    "irrelevant": {
      label: "Irrelevant",
      id: 3,
      count: 0
    }
  }

  @Input() feedCountItem = {} as FeedsCountModel;

  public selectedStatusButton: string = this.statusButtons.pending_review.label;

  /**
   * Here wee ned to add the DI and update the date range picker options.
   * 
   * @param daterangepickerOptions date range picker Options
   * @param _feedsService feeds service
   * @param _filterService filter Service
   * @param _filterService filter Service
   */
  constructor(private daterangepickerOptions: DaterangepickerConfig,
    private _feedsService: FeedsService,
    private _filterService: FilterService,
    private _globalService: GlobalService) {
    this.daterangepickerOptions.settings = {
      locale: { format: 'YYYY-MM-DD' },
      alwaysShowCalendars: false,
      "opens": "right",
      ranges: {
        'Today': [moment().subtract(0, 'day'), moment()],
        'Yesterday': [moment().subtract(1, 'day'), moment().subtract(1, 'day')],
        'Last 3 days': [moment().subtract(2, 'day'), moment()],
        'Last 7 Days': [moment().subtract(6, 'day'), moment()],
        'Last 30 days': [moment().subtract(1, 'month'), moment()]
      }
    };
  }

  /**
   * Here we need to update the feeds filter object and call the setFeedsFilterObj
   * function which will helpful for getting latest feeds with filters.
   * 
   * @param selectedPageNumber selected Page Number
   * @param reviewed_decision_id reviewed decision id
   * @param relevancy relevancy
   */
  updateFeedsFilterObject(selectedPageNumber: string, reviewed_decision_id?: string, relevancy?: string): void {
    if (selectedPageNumber === DEFAULT_PAGE) {
      this._filterService.clearFeedsFilterObj();
      this._filterService.sendsSlectedOriginatorId(this.defaultOriginatorId);
    }
    const feedFilteredObj = this._filterService.getFeedsFilterObj();
    if (reviewed_decision_id != undefined) {
      feedFilteredObj.reviewed_decision_id = reviewed_decision_id;
    }
    if (relevancy != undefined) {
      feedFilteredObj.relevancy = relevancy;
    }
    feedFilteredObj.page = selectedPageNumber;
    this._filterService.setFeedsFilterObj(feedFilteredObj);
  }

  /**
   * In this lifecycle method we are doing below things
   * 1. based upon the selected page observable data we are calling the feed filter subject
   * 2. We are mapping counts to the UI
   * 3.Getting all feeds count and mapping to UI.
   */
  ngOnInit(): void {
    const UNDEFINED = undefined;
    this.emitSelectedStatusBtn(this.statusButtons.pending_review.label);
    this._filterService.getSelectedPageAsObservable()
      .subscribe(selectedPageNumber => {
        switch (this.selectedStatusButton) {
          case this.statusButtons.pending_review.label:
            this.updateFeedsFilterObject(selectedPageNumber, "30", UNDEFINED);
            break;
          case this.statusButtons.reviewed.label:
            this.updateFeedsFilterObject(selectedPageNumber, "40", UNDEFINED);
            break;
          case this.statusButtons.relevant.label:
            this.updateFeedsFilterObject(selectedPageNumber, UNDEFINED, "1");
            break;

          case this.statusButtons.irrelevant.label:
            this.updateFeedsFilterObject(selectedPageNumber, UNDEFINED, "0");
            break;
        }
      });
    this.mapCountsToCoutFeeds();
    this.getFeedsCount();
  }

  /**
   * Get feeds count and map to the UI.
   */
  getFeedsCount(): void {
    this._feedsService.fetchAllFeedCount(0)
      .subscribe((data: FeedsCountModel) => {
        this._feedsService.setFeedCount(data);
      })
  }

  private mapCountsToCoutFeeds() {
    this._feedsService.getFeedsCountSubject().subscribe(
      data => {
        this.statusButtons.pending_review.count = data.pending;
        this.statusButtons.reviewed.count = data.reviewed;
        this.statusButtons.relevant.count = data.relevent;
        this.statusButtons.irrelevant.count = data.irrelevent;
      })
  }

  /**
   * This function will help to emit the selected status button.
   * @param selectedStatusButton selected status button, get a single value from statusButton Array.
   */
  emitSelectedStatusBtn(selectedStatusButton: string): void {
    this.selectedStatusBtnEvent.emit(selectedStatusButton);
  }

  /**
   * Get Pending feeds depends upon the status button.
   */
  getPendingFeeds(): void {
    this.selectedStatusButton = this.statusButtons.pending_review.label;
    this.emitSelectedStatusBtn(this.selectedStatusButton);
    this._filterService.sendSelectedPageBSubject(1);
  }

  /**
   * Get reviewed feeds depends upon the status button.
   */
  getReviewedFeeds(): void {
    this.selectedStatusButton = this.statusButtons.reviewed.label;
    this.emitSelectedStatusBtn(this.selectedStatusButton);
    this._filterService.sendSelectedPageBSubject(1);
  }

  /**
   * Get irrelevant feeds depends upon the status button.
   */
  getRelevantFeeds(): void {
    this.selectedStatusButton = this.statusButtons.relevant.label;
    this.emitSelectedStatusBtn(this.selectedStatusButton);
    this._filterService.sendSelectedPageBSubject(1);
  }

  /**
   * Get relevant feeds depend upon the status button.
   */
  getIrrelevantFeeds(): void {
    this.selectedStatusButton = this.statusButtons.irrelevant.label;
    this.emitSelectedStatusBtn(this.selectedStatusButton);
    this._filterService.sendSelectedPageBSubject(1);
  }


  public chosenDate: { start: moment.Moment, end: moment.Moment } = {
    start: moment().subtract(12, 'month'),
    end: moment().subtract(6, 'month'),
  };

  public dateSelectedlabel = "Select Date";

  public picker1 = {
    opens: 'left',
    startDate: moment().subtract(-1, 'day'),
    endDate: moment().subtract(-1, 'day')
  }

  /**
   * This function will call when any date is selected from UI.
   * 
   * @param value date value
   * @param dateInput start and end value of date
   */
  public selectedDate(value: any, dateInput: any): void {
    dateInput.start = value.start;
    dateInput.end = value.end;
    const feedFilteredObj = this._filterService.getFeedsFilterObj();
    feedFilteredObj.start_date = this._globalService.convertDate(new Date(value.start));
    feedFilteredObj.end_date = this._globalService.convertDate(new Date(value.end));
    feedFilteredObj.page = DEFAULT_PAGE;
    feedFilteredObj.originator_id = BLANK;
    this._filterService.sendsSlectedOriginatorId(BLANK);
    const selectedMailBox = (feedFilteredObj.shared_mailbox_id == undefined) ? 0 : parseInt(feedFilteredObj.shared_mailbox_id);
    this.getAllFeedCount(selectedMailBox, feedFilteredObj.start_date, feedFilteredObj.end_date);
    this._filterService.scrollToTopFeedList();
    this._filterService.setFeedsFilterObj(feedFilteredObj)
    this.dateSelectedlabel = value.label;
  }

  /**
   * this function will be helpfull when we need to update the setting of date
   * this will take as array of date duration
   */
  public updateSettings(): void {
    this.daterangepickerOptions.settings.locale = { format: 'YYYY/MM/DD' };
    this.daterangepickerOptions.settings.ranges = {
      'Today': [moment().subtract(0, 'day'), moment()],
      'Yesterday': [moment().subtract(1, 'day'), moment().subtract(1, 'day')],
      'Last 3 days': [moment().subtract(2, 'day'), moment()],
      'Last 7 Days': [moment().subtract(6, 'day'), moment()],
      'Last 30 days': [moment().subtract(1, 'month'), moment()]
    };
  }

  /**
   * This function will help to get all the feed counts.
   * 
   * @param selectedMailBox selected mailbox
   * @param start_date start date
   * @param end_date end date
   */
  getAllFeedCount(selectedMailBox: number, start_date?: string, end_date?: string): void {
    this._feedsService.fetchAllFeedCount(selectedMailBox, start_date, end_date)
      .subscribe(
        data => {
          this._feedsService.setFeedCount(data);
        }
      )
  }

}
