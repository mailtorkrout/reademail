import { Component, OnInit, Output } from '@angular/core';
import { FeedsService } from '../../../services/feeds.service';
import { IFeed } from '../../../models/feedModel';
import { FeedsCountModel } from '../../../models/FeedsCountModel';
import { ReviewSubmitModel } from 'src/app/models/ReviewSubmitModel';
import { NotifierService } from 'angular-notifier';
import { BLANK, DEFAULT_PAGE, NULL_VALUE } from 'src/app/app.config';
import { GlobalService } from 'src/app/services/global.service';
import { FilterService } from 'src/app/services/feeds.filter.service';
import { FilterModel } from 'src/app/models/filterModel';
@Component({
  selector: 'app-my-work',
  templateUrl: './my-work.component.html',
  styleUrls: ['./my-work.component.sass']
})

export class MyWorkComponent implements OnInit {

  public feedDetail!: IFeed;
  public selectedStatusButton = BLANK;
  public feedsCount: FeedsCountModel = {} as FeedsCountModel;

  public is_future_date_selected = false;
  public is_email_domain_selected = false;
  public is_keywords_selected = false;

  public msg_from: string = BLANK;

  public msg_body_translated: string = BLANK;
  public msg_subject: string = BLANK;

  public reason:string = BLANK;

  constructor(
    private _feedsService: FeedsService,
    private _filterService: FilterService,
    private _notifierService: NotifierService,
    private _globalService: GlobalService) {

  }

  setSelectedStatusButton(selectedStatusButtonLabel: string): void {
    this.selectedStatusButton = selectedStatusButtonLabel;
  }

  ngOnInit(): void {
    this._feedsService.getFeedDetail().subscribe(
      data => {
        this.feedDetail = data;
        this.msg_from = data.msg_from;
        this.is_future_date_selected = false;
        this.is_email_domain_selected = false;
        this.is_keywords_selected = false;
        this.msg_body_translated = data.msg_body_translated;
        this.msg_subject = (' ' + data.msg_subject).slice(1);
        const foreign_lang = document.getElementById("foreign_lang");
        if (foreign_lang != undefined)
          document.getElementById("foreign_lang")?.classList.add('active');

      }
    )

  }

  getAllFeedCount(selectedMailBox: number, start_date?: string, end_date?: string): void {
    this._feedsService.fetchAllFeedCount(selectedMailBox, start_date, end_date)
      .subscribe(
        data => {
          this._feedsService.setFeedCount(data);
        }
      )
  }

  submitReview(status: string, feedDetail: IFeed): void {
    const element = (document.getElementById('reason')as HTMLInputElement).value;
    feedDetail.review_comments= element;
    if(status=="20"){
      if(element!=null){
        const data: ReviewSubmitModel = {} as ReviewSubmitModel;
        const uid = (localStorage.getItem('uid') ? localStorage.getItem('uid') : NULL_VALUE);
        data.review_date = NULL_VALUE;
        data.reviewed_by = uid ? parseInt(uid) : data.reviewed_by;
        data.reviewed_decision_id = status;
        data.uuid = feedDetail.uuid;
        data.review_comments = feedDetail.review_comments;
        this._feedsService.submitReview(data)
          .subscribe(
            () => {
              const feedFilteredObj: FilterModel = this._filterService.getFeedsFilterObj();

              console.log("feedFilteredObj", feedFilteredObj);
              let selectedMailBox = parseInt(feedFilteredObj.shared_mailbox_id);
              selectedMailBox = (selectedMailBox !== undefined) ? selectedMailBox : 0;

              if (feedFilteredObj.start_date != undefined && feedFilteredObj.end_date != undefined) {
                this.getAllFeedCount(selectedMailBox, feedFilteredObj.start_date, feedFilteredObj.end_date);
              } else {
                this.getAllFeedCount(selectedMailBox);
              }
              console.log("selectedMailBox", selectedMailBox);
              if (isNaN(selectedMailBox)) {
                feedFilteredObj.shared_mailbox_id = "";
              } else {
                feedFilteredObj.shared_mailbox_id = selectedMailBox === 0 ? BLANK : selectedMailBox.toString();
              }
              feedFilteredObj.page = DEFAULT_PAGE;
              this._filterService.sendsSlectedOriginatorId(feedFilteredObj.originator_id);
              this._filterService.scrollToTopFeedList();
              this._filterService.setFeedsFilterObj(feedFilteredObj);
            });
            this.reason="";
      }
    }
    else{
    const data: ReviewSubmitModel = {} as ReviewSubmitModel;
    const uid = (localStorage.getItem('uid') ? localStorage.getItem('uid') : NULL_VALUE);
    data.review_date = NULL_VALUE;
    data.reviewed_by = uid ? parseInt(uid) : data.reviewed_by;
    data.reviewed_decision_id = status;
    data.uuid = feedDetail.uuid;
    this._feedsService.submitReview(data)
      .subscribe(
        () => {
          const feedFilteredObj: FilterModel = this._filterService.getFeedsFilterObj();

          console.log("feedFilteredObj", feedFilteredObj);
          let selectedMailBox = parseInt(feedFilteredObj.shared_mailbox_id);
          selectedMailBox = (selectedMailBox !== undefined) ? selectedMailBox : 0;

          if (feedFilteredObj.start_date != undefined && feedFilteredObj.end_date != undefined) {
            this.getAllFeedCount(selectedMailBox, feedFilteredObj.start_date, feedFilteredObj.end_date);
          } else {
            this.getAllFeedCount(selectedMailBox);
          }
          console.log("selectedMailBox", selectedMailBox);
          if (isNaN(selectedMailBox)) {
            feedFilteredObj.shared_mailbox_id = "";
          } else {
            feedFilteredObj.shared_mailbox_id = selectedMailBox === 0 ? BLANK : selectedMailBox.toString();
          }
          feedFilteredObj.page = DEFAULT_PAGE;
          this._filterService.sendsSlectedOriginatorId(feedFilteredObj.originator_id);
          this._filterService.scrollToTopFeedList();
          this._filterService.setFeedsFilterObj(feedFilteredObj);
        });
  }
  }
  updateCountNFeeds(uuid: string): void {
    this._notifierService.show({
      type: 'success',
      message: 'Successfully Reviewed'
    });

    const feedCount: FeedsCountModel = this._feedsService.getFeedsCount();
    feedCount.pending = feedCount.pending - 1;
    feedCount.reviewed = feedCount.reviewed + 1;
    this._feedsService.setFeedCount(feedCount);
    this._feedsService.removeFeedsByUid(uuid);
  }

  highlightKeyWords(feedDetail: IFeed): void {
    this.is_future_date_selected = false;
    if (this.is_email_domain_selected)
      this.highlightEmailNDomain(feedDetail);
    this.is_keywords_selected = !this.is_keywords_selected;
    this._globalService.highlightKeywords(feedDetail.discovered_keywords, feedDetail.msg_body_HTML, this.is_keywords_selected);
    this.msg_body_translated = this._globalService.highlightKeywordsInTranslatedBody(feedDetail.discovered_keywords, feedDetail.msg_body_translated, this.is_keywords_selected);
    this.msg_subject = this._globalService.highlightSubject(feedDetail.discovered_keywords, feedDetail.msg_subject, this.is_keywords_selected);
  }

  highlightFutureDate(feedDetail: IFeed): void {
    this.is_keywords_selected = false;
    if (this.is_email_domain_selected)
      this.highlightEmailNDomain(feedDetail);
    this.is_future_date_selected = !this.is_future_date_selected;
    this._globalService.highlightFutureDates(feedDetail.discovered_dates, feedDetail.msg_body_HTML, this.is_future_date_selected);
    this.msg_body_translated = this._globalService.highlightKeywordsInTranslatedBody(feedDetail.discovered_dates, feedDetail.msg_body_translated, this.is_future_date_selected);
    this.msg_subject = this._globalService.highlightSubject(feedDetail.discovered_dates, feedDetail.msg_subject, this.is_future_date_selected);
  }

  highlightEmailNDomain(feedDetail: IFeed): void {
    this._globalService.highlightKeywords(feedDetail.discovered_keywords, feedDetail.msg_body_HTML, false);
    this.msg_body_translated = this._globalService.highlightKeywordsInTranslatedBody(feedDetail.discovered_keywords, feedDetail.msg_body_translated, false);
    this.msg_subject = this._globalService.highlightSubject(feedDetail.discovered_keywords, feedDetail.msg_subject, false);
    this.is_keywords_selected = false;
    this.is_future_date_selected = false;
    this.is_email_domain_selected = !this.is_email_domain_selected;
    this.msg_from = this._globalService.highlightEmailNDomain(feedDetail.msg_from, this.is_email_domain_selected);

  }


}
