import { Component, OnInit } from '@angular/core';
import { faEllipsisV } from '@fortawesome/free-solid-svg-icons';
import { DEFAULT_PAGE, BLANK } from 'src/app/app.config';
import { FilterService } from './../../../../services/feeds.filter.service';
import { FeedsService } from './../../../../services/feeds.service';


@Component({
  selector: 'app-feed-list-top-menu',
  templateUrl: './feed-list-top-menu.component.html',
  styleUrls: ['./feed-list-top-menu.component.sass']
})
export class FeedListTopMenuComponent implements OnInit {

  faEllipsisV = faEllipsisV;

  selectedOriginatorId = BLANK;

  countOfSelectedFilter = 0;

  countSelectedFilterStr = BLANK;

  public originatorValues: {
    value: string,
    name: string
  }[] =
    [
      { value: "1", name: "Email" },
      { value: "0", name: "RSS Feed" },
      { value: BLANK, name: "All" }
    ];
  constructor(private _filterService: FilterService,
    private _feedsService: FeedsService) {

  }

  /**
   * On change on originator type.
   * @param data $event
   */
  onOriginatorTypeChange(): void {
    this._filterService.scrollToTopFeedList();
    const feedFilteredObj = this._filterService.getFeedsFilterObj();
    feedFilteredObj.originator_id = this.selectedOriginatorId;
    feedFilteredObj.page = DEFAULT_PAGE
    this._filterService.setFeedsFilterObj(feedFilteredObj)
  }

  ngOnInit(): void {
    this._filterService.getSlectedOriginatorId()
      .subscribe(slectedOriginatorId => {
        if (slectedOriginatorId != undefined) {
          this.selectedOriginatorId = slectedOriginatorId;
        } else {
          this.selectedOriginatorId = BLANK;
        }
      });

    this._feedsService.getTotalRecordsBSubjectAsObservable()
      .subscribe(totalPages => {
        this.countOfSelectedFilter = totalPages;
        if (this.selectedOriginatorId == BLANK) {
          this.countSelectedFilterStr = `${totalPages} Total`;
        }
        else if (this.selectedOriginatorId == "1") {
          this.countSelectedFilterStr = `${totalPages} Emails`;
        }
        else if (this.selectedOriginatorId == "0") {
          this.countSelectedFilterStr = `${totalPages} RSS Feeds`;
        }
      });

  }

}
