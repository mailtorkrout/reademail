/* eslint-disable @typescript-eslint/no-unused-vars */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FeedsService } from 'src/app/services/feeds.service';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { IFeed } from 'src/app/models/feedModel';
import { GlobalService } from 'src/app/services/global.service';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FeedListTopMenuComponent } from './feed-list-top-menu.component';
import { FilterService } from 'src/app/services/feeds.filter.service';
import { BLANK } from 'src/app/app.config';
import { FormsModule } from '@angular/forms';

describe('FeedListTopMenuComponent', () => {
    let component: FeedListTopMenuComponent;
    let fixture: ComponentFixture<FeedListTopMenuComponent>;
    let FeedsServiceStub: Partial<FeedsService>;
    let feedService: FeedsService;
    let filterService: FilterService;
    let mockGlobalService: GlobalService;
    let httpClientSpy: { get: jasmine.Spy };
    let mockFeedDetail: IFeed;

    beforeEach(async () => {
        httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post']);
        feedService = new FeedsService(httpClientSpy as any, mockGlobalService);
        filterService = new FilterService(feedService, mockGlobalService);

        mockFeedDetail = {
            "batch_id": 1038,
            "discovered_dates": ["year"],
            "discovered_keywords": ['year'],
            "future_date_score": 1.00,
            "is_actual_html": true,
            "keyword_score": 1.52,
            "listed_domain": false,
            "msg_body": "test message body",
            "msg_body_HTML": "Test message HTML",
            "msg_body_translated": BLANK,
            "msg_from": "donotreply@wordpress.com",
            "msg_id": "<59947194.47732.0@wordpress.com>",
            "msg_received_dt": new Date(),
            "msg_subject": "[New post] Critical Missing Person Richard Green",
            "msg_to": [
                "aptexas@ap.org"
            ],
            "originator_id": 1,
            "relevancy": true,
            "reviewed_date": new Date(),
            "reviewed_decision_id": 30,
            "reviewedby_user_id": 1,
            "search_score": 3.21,
            "shared_mailbox_id": 504,
            "total_score": 4.21,
            "uuid": "71734d75-e87b-11eb-a02c-33d8c57487e9",
            "reviewed_by_username": BLANK,
            "review_comments":BLANK,
            "msg_language": "en"
        };

        FeedsServiceStub = {
            getTotalRecordsBSubjectAsObservable() {
                return of(1);
            },
            fetchAllFeedsWithFilter(url: string) {
                return of({
                    "mails": [
                        mockFeedDetail
                    ],
                    "max_records": 10,
                    "total_pages": 1,
                    "total_records": 1
                })
            },
            sendTotalRecordsBSubject(data: number) {
                spyOn(feedService, 'sendTotalRecordsBSubject').and.callThrough();
                feedService.sendTotalRecordsBSubject(data);
            },
            setTotalPages(data: number) {
                spyOn(feedService, 'setTotalPages').and.callThrough();
                feedService.setTotalPages(data);
            },
            setFeeds(feeds: IFeed[]) {
                spyOn(feedService, 'setFeeds').and.callThrough();
                feedService.setFeeds(feeds);
            },
            sendFeeds() {
                spyOn(feedService, 'sendFeeds').and.callThrough();
                feedService.sendFeeds();
            }
        };

        await TestBed.configureTestingModule({
            declarations: [FeedListTopMenuComponent],
            providers: [
                {
                    provide: FeedsService,
                    useValue: FeedsServiceStub
                }
            ],
            imports: [RouterTestingModule, FormsModule],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        }).compileComponents();

    });

    beforeEach(() => {
        fixture = TestBed.createComponent(FeedListTopMenuComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create feedlist top menu component', () => {
        expect(component).toBeTruthy();
        expect(component.countSelectedFilterStr).toBe("1 Total")
    });

    it('should create feedlist top menu component when originator id =1', () => {
        component.selectedOriginatorId = "1";
        component.ngOnInit();
        expect(component.countSelectedFilterStr).toBe("1 Emails")
    });

    it('should create feedlist top menu component when originator id =0', () => {
        component.selectedOriginatorId = "0";
        component.ngOnInit();
        expect(component.countSelectedFilterStr).toBe("1 RSS Feeds")
    });

    it('on originator type change', () => {
        spyOn(filterService, 'setFeedsFilterObj').and.callThrough();
        component.onOriginatorTypeChange();
    });

});
