import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MailBoxListModel } from 'src/app/models/MailBoxModel';
import { MailboxService } from './../../services/mailbox.service'
import { UserDetailService } from './../../services/userDetail.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})

export class HomeComponent implements OnInit {

  /**
   * 
   * Here we are injecting all dependencies.
   * 
   * @param _activatedRoute activated Route
   * @param _activatedRoute activated Route
   * @param _userDetailService user Detail Service
   */
  constructor(
    private _activatedRoute: ActivatedRoute,
    private _mailboxService: MailboxService,
    private _userDetailService: UserDetailService) {

  }
  public mailBoxList: MailBoxListModel = {} as MailBoxListModel;

  /**
   * On home component start this function will call by default.
   */
  ngOnInit(): void {
    this.mailBoxList = this._activatedRoute.snapshot.data['mailBoxList'];
    this._mailboxService.setMailBoxList(this.mailBoxList);
    this._userDetailService.setUserId(parseInt(this.mailBoxList?.user_id));
  }

}


