import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { MailBoxListModel } from 'src/app/models/MailBoxModel';
import { MailboxService } from 'src/app/services/mailbox.service';
import { UserDetailService } from 'src/app/services/userDetail.service';
import { HomeComponent } from './home.component';
import { RouterTestingModule } from '@angular/router/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let mockMailBoxList: MailBoxListModel;
  let mailBoxServiceStub: Partial<MailboxService>;
  let uid: number;
  let userDetailServiceStub: Partial<UserDetailService>;
  let mockMailboxlistsetByService: MailBoxListModel;



  beforeEach(async () => {

    const fakeActivatedRoute = {
      snapshot: { data: {} }
    } as ActivatedRoute;
    fakeActivatedRoute.snapshot.data['mailBoxList'] = mockMailBoxList;

    mockMailBoxList = {
      "mailboxes":
        [
          {
            "mailbox_id": 503,
            "mailbox_name": "demo-fwd-planning@gmail.com"
          },
          {
            "mailbox_id": 504,
            "mailbox_name": "demo2@gmail.com"
          }
        ],
      "user_id": "107"
    };



    mailBoxServiceStub = {
      getMailBoxList(): MailBoxListModel {
        return mockMailboxlistsetByService;
      },
      setMailBoxList(mailBoxList: MailBoxListModel): void {
        mockMailboxlistsetByService = mailBoxList;
      }
    };

    userDetailServiceStub = {
      getUserId(): number {
        return uid;
      },
      setUserId(data: number): void {
        uid = data;
      },
    };

    await TestBed.configureTestingModule({
      declarations: [HomeComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: fakeActivatedRoute
        },
        {
          provide: MailboxService,
          useValue: mailBoxServiceStub
        },
        {
          provide: UserDetailService,
          useValue: userDetailServiceStub
        }
      ],
      imports: [RouterTestingModule],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    }).compileComponents();

    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create home component', () => {
    expect(component).toBeTruthy();
  });

  it("should get mailboxlist ngOnit life cycle", fakeAsync(() => {
    component.ngOnInit();
    tick();
    if (component.mailBoxList != undefined) {
      expect(component.mailBoxList).toEqual(mockMailBoxList);
    }
    else
      expect(component.mailBoxList).toEqual(undefined);
  }))

});
