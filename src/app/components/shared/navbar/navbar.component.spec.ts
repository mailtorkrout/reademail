import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { UserDetailService } from 'src/app/services/userDetail.service';
import { FeedsService } from 'src/app/services/feeds.service';
import { NavbarComponent } from './navbar.component';
import { RouterTestingModule } from '@angular/router/testing';
import { UserDetailCookieModel } from 'src/app/models/userDetailCookieModel';
import { Observable, of } from 'rxjs';
import { FeedsCountModel } from 'src/app/models/FeedsCountModel';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('NavbarComponent', () => {
  let component: NavbarComponent;
  let fixture: ComponentFixture<NavbarComponent>;
  let userDetailServiceStub: Partial<UserDetailService>;
  let FeedsServiceStub: Partial<FeedsService>;

  beforeEach(async () => {

    userDetailServiceStub = {
      getUserDetailFromCookie(): UserDetailCookieModel {
        return {
          "aud": "string",
          "iss": "string",
          "iat": "string",
          "nbf": "string",
          "exp": "string",
          "aio": "string",
          "name": "Ratikanta Rout",
          "nonce": "string",
          "oid": "string",
          "preferred_username": "ratikanta@gmail.com",
          "uname": "Ratikanta Rout",
          "rh": "string",
          "sub": 'string',
          "tid": "string",
          "uti": "string",
          "ver": "string",
          "uid": 10
        }
      }
    };

    FeedsServiceStub = {
      getFeedsCountSubject(): Observable<FeedsCountModel> {
        const mockFeedCountModel = {} as FeedsCountModel;
        mockFeedCountModel.acccepted = 10;
        mockFeedCountModel.rejected = 5;
        mockFeedCountModel.relevent = 7;
        mockFeedCountModel.irrelevent = 13;
        mockFeedCountModel.pending = 20;
        return of(mockFeedCountModel);
      }
    };

    await TestBed.configureTestingModule({
      declarations: [NavbarComponent],
      providers: [
        {
          provide: FeedsService,
          useValue: FeedsServiceStub
        },
        {
          provide: UserDetailService,
          useValue: userDetailServiceStub
        }
      ],
      imports: [RouterTestingModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  });

  // beforeEach(() => {
  //   fixture = TestBed.createComponent(NavbarComponent);
  //   component = fixture.componentInstance;
  //   fixture.detectChanges();
  // });

  // it('should create navbar component', () => {
  //   expect(component).toBeTruthy();
  // });


  // it("name and user name should match with stub after ng oninit", fakeAsync(() => {
  //   fixture.whenRenderingDone().then(() => {
  //     component.ngOnInit();
  //     fixture.detectChanges();
  //     expect(component.name).toBe("Ratikanta Rout");
  //     expect(component.username).toBe("ratikanta@gmail.com");
  //   });

  // }))

  // it("name and user name should match with stub after ng oninit", fakeAsync(() => {
  //   fixture.whenRenderingDone().then(() => {
  //     component.ngOnInit();
  //     fixture.detectChanges();
  //     expect(component.name).toBe("Ratikanta Rout");
  //     expect(component.username).toBe("ratikanta@gmail.com");
  //   })
  // }))

  // it("pending mail count should be same as stub after ng oninit", fakeAsync(() => {
  //   component.ngOnInit();
  //   tick(0, { processNewMacroTasksSynchronously: false });
  //   fixture.detectChanges();
  //   expect(component.pendingCount).toBe(20);
  // }))

});
