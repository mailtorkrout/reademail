import { Component, OnInit } from '@angular/core';
import { faUser as fasUser, faSignOutAlt } from '@fortawesome/free-solid-svg-icons';
import { faBell } from '@fortawesome/free-regular-svg-icons';
import { UserDetailService } from '../../../services/userDetail.service';
import { FeedsService } from '../../../services/feeds.service';
import { BLANK, DEFAULT_PAGE } from 'src/app/app.config';
import { FilterService } from 'src/app/services/feeds.filter.service';
import { MailboxService } from 'src/app/services/mailbox.service';
import { MailBoxListModel, MailBoxModel } from 'src/app/models/MailBoxModel';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.sass']
})
export class NavbarComponent implements OnInit {

  faBell = faBell;
  fasUser = fasUser;
  faSignOut = faSignOutAlt;

  public pendingCount = 0;
  public name = BLANK;
  public username = BLANK;

  public selectedMailBox = 0;
  public mailBoxes: MailBoxModel[] = [];

  constructor(
    private _userDetailService: UserDetailService,
    private _filterService: FilterService,
    private _mailboxService: MailboxService,
    private _feedsService: FeedsService) { }

  ngOnInit(): void {
    const account = this._userDetailService.getUserDetailFromCookie();
    this.name = account?.name;
    this.username = account?.preferred_username;
    this._feedsService.getFeedsCountSubject().subscribe(
      (data) => {
        this.pendingCount = data.pending;
      }
    );
    this.getMailBoxes();
  }

  getMailBoxes(): void {
    const mailBoxeList: MailBoxListModel = this._mailboxService.getMailBoxList();
    this.mailBoxes = mailBoxeList.mailboxes;
  }

  getAllFeedCount(selectedMailBox: number, start_date?: string, end_date?: string): void {
    this._feedsService.fetchAllFeedCount(selectedMailBox, start_date, end_date)
      .subscribe(
        data => {
          this._feedsService.setFeedCount(data);
        }
      )
  }

  onMailBoxChangeGetFeedsCount(): void {
    const feedFilteredObj = this._filterService.getFeedsFilterObj();
    if (feedFilteredObj.start_date != undefined && feedFilteredObj.end_date != undefined) {
      this.getAllFeedCount(this.selectedMailBox, feedFilteredObj.start_date, feedFilteredObj.end_date);
    } else {
      this.getAllFeedCount(this.selectedMailBox);
    }
    feedFilteredObj.shared_mailbox_id = this.selectedMailBox === 0 ? BLANK : this.selectedMailBox.toString();
    feedFilteredObj.page = DEFAULT_PAGE;
    feedFilteredObj.originator_id = BLANK;
    this._filterService.sendsSlectedOriginatorId(BLANK);
    this._filterService.scrollToTopFeedList();
    this._filterService.setFeedsFilterObj(feedFilteredObj)
  }

}
