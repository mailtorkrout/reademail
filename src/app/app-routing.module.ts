import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MyWorkComponent } from './components/pages/my-work/my-work.component';
import { HomeComponent } from './components/home/home.component';
import { MailboxResolver } from './resolver/mailbox.resolver';

const routes: Routes = [

  {
    path: '', component: HomeComponent,

    children: [
      {
        path: '',
        component: MyWorkComponent,
      },
      {
        path: 'my-work',
        component: MyWorkComponent,
      }
    ],
    resolve: {
      mailBoxList: MailboxResolver
    }
  },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
