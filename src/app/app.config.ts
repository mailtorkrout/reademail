export const DEFAULT_PAGE = "1";
export const ONE_NUMBER = 1;
export const ZERO_STRING = "0";
export const BLANK = "";
export const NULL_VALUE = null;