import { BLANK } from './app.config';

/**
 * Get the base url.
 * @returns baseurl
 */
export function getbaseUrl(): string {
  const baseUrl = window.location.origin;
  if (baseUrl.indexOf('localhost') === -1) {
    return BLANK;
  } else {
    return 'http://localhost:4200';
  }
}
