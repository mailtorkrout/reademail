

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Daterangepicker } from 'ng2-daterangepicker';

import { MyWorkComponent } from './components/pages/my-work/my-work.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { CountFeedsComponent } from './components/pages/my-work/count-feeds/count-feeds.component';
import { FeedListComponent } from './components/pages/my-work/feed-list/feed-list.component';
import { FeedListTopMenuComponent } from './components/pages/my-work/feed-list-top-menu/feed-list-top-menu.component';
import { NotifierModule } from 'angular-notifier';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoaderInterceptor } from "./interceptor/loader.interceptor";
import { HomeComponent } from './components/home/home.component';
import { MailboxResolver } from './resolver/mailbox.resolver';

@NgModule({
  declarations: [
    AppComponent,
    MyWorkComponent,
    NavbarComponent,
    CountFeedsComponent,
    FeedListComponent,
    FeedListTopMenuComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    Daterangepicker,
    HttpClientModule,
    NotifierModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoaderInterceptor,
      multi: true
    },
    MailboxResolver
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
