/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { Injectable } from '@angular/core';
import { BLANK } from '../app.config';
import { IFeed } from '../models/feedModel';
import * as $ from 'jquery';

declare let require: any;
// eslint-disable-next-line @typescript-eslint/no-var-requires
const Mark = require('node_modules/mark.js/dist/mark.js');

@Injectable({
    providedIn: 'root'
})
export class GlobalService {

    public convertDate(date: Date): string {
        date = new Date(date);
        const day = ('0' + date.getDate()).slice(-2);
        const month = ('0' + (date.getMonth() + 1)).slice(-2);
        const year = date.getFullYear();
        return year + '-' + month + '-' + day;
    }

    private sanitizWithUnicodesData(data: string): string {
        return data.replace(/\\xc2\\xb7/g, "&#xB7;")
            .replace(/\\xc3\\xb3/g, "&#xF3;")
            .replace(/\\xc3\\xa1/g, "&#xE1;")
            .replace(/\\xc2\\xab/g, "&#xAB;")
            .replace(/\\xc2\\xbb/g, "&#xBB;")
            .replace(/\\xc3\\xba/g, "&#xFA;")
            .replace(/\\xc3\\xa9/g, "&#xE9;")
            .replace(/\\xc3\\xad/g, "&#xED;")
            .replace(/\\xc3\\xb1/g, "&#xF1;")
            .replace(/\\'/g, "'")
            .replace(/\\x93/g, "&#x0093;")
            .replace(/\\x92/g, "&#x0092;")
            .replace(/\\xb7/g, "&middot;")
            .replace(/\\xe2/g, "&acirc;")
            .replace(/\\x80/g, "&#x0080;")
            .replace(/\\x98/g, "&#x0098;")
            .replace(/\\x99/g, "&#x0099;")
            .replace(/\\x96/g, "&#x0096;")
            .replace(/\\x94/g, "&#x0094;")
            .replace(/\\xa6/g, "&brvbar;")
            .replace(/\\xa2/g, "&cent;")
            .replace(/\\x8b/g, "&#x008b;")
            .replace(/\\x8c/g, "&#x008c;")
            .replace(/\\xc2/g, "&Acirc;")
            .replace(/\\xa9/g, "&copy;")
            .replace(/\\xef/g, "&iuml;")
            .replace(/\\xbb/g, "&raquo;")
            .replace(/\\xbf/g, "&iquest;")
            .replace(/\\xaf/g, "&macr;")
            .replace(/\\xa0/g, "&nbsp;")
            .replace(/&nbsp;/g, " ")
            .replace(/\\x84/g, "&#x0084;")
            .replace(/\\x9c/g, "&#x009c;")
            .replace(/\\x9d/g, "&#x009d;")
            .replace(/\\xc3/g, "&Atilde;")
            .replace(/\\x91/g, "&#x0091;")
            .replace(/<sup>|<\/sup>|<\/strong><strong>/g, '')
    }

    /**
     * //https://charbase.com/00af-unicode-macron
     * @param data
     * @param type
     * @returns
     */
    public sanitizeHtml(data: string, type: number): string {
        if (data != undefined) {
            data = data.replace(/\\r/g, BLANK);
            data = data.replace(/\\n/g, BLANK);
            data = data.replace(/\\t/g, BLANK);
            data = this.sanitizWithUnicodesData(data);
            if (type === 1 && data.substring(0, 2) === "b'")
                data = data.substring(2);
            data = data.slice(0, -1);
            return data;
        }
        return data;
    }

    /**
 * Get the Querystring result
 *
 * @returns string urlQueryStr
 */
    public getQueryString(data: any): string {
        let urlQueryStr = BLANK;
        for (const [key, value] of Object.entries(data)) {
            if (value !== BLANK && value != undefined) {
                const query: string = key + "=" + value;
                if (urlQueryStr !== BLANK) {
                    urlQueryStr = urlQueryStr + "&" + query;
                } else {
                    urlQueryStr = query;
                }
            }
        }
        if (urlQueryStr !== BLANK)
            return "?" + urlQueryStr;
        else
            return urlQueryStr;
    }

    public highlightEmailNDomain(msg_from: string, highlight: boolean): string {
        const msgFrom = (' ' + msg_from).slice(1);
        if (!highlight)
            return msgFrom;
        return "<span class='bg-yellow'>" + " " + msgFrom + " " + "</span>";
    }


    public highlightFutureDates(discovered_dates: string[], msg_body_HTML: string, flag: boolean): void {
        this.writeToFrameBody(msg_body_HTML);
        if (!flag) {
            return;
        }
        const options = {
            "separateWordSearch": false,
            "accuracy": "partially",
            "diacritics": true,
            "acrossElements": true,
            "caseSensitive": true,
            "ignoreJoiners": true,
            "exclude": [],
            "ignorePunctuation": [],
            "synonyms": {}
        };

        let discoveredDates = [...new Set(discovered_dates)];
        discoveredDates = discoveredDates.sort((a, b) => {
            return b.length - a.length;
        });

        const iFrameElem: any = document.getElementById('iframe_original_desc');
        if (iFrameElem != undefined) {
            const doc = iFrameElem.contentWindow.document;
            const markInstance = new Mark(doc);
            markInstance.unmark({
                done: function () {
                    markInstance.mark(discoveredDates, options);
                }
            });
        }
    }

    public highlightKeywords(discovered_keywords: string[], msg_body_HTML: string, flag: boolean): void {
        this.writeToFrameBody(msg_body_HTML);
        if (!flag) {
            return;
        }

        const options = {
            "separateWordSearch": false,
            "accuracy": "partially",
            "diacritics": true,
            "acrossElements": true,
            "caseSensitive": false,
            "ignoreJoiners": true,
            "exclude": [],
            "ignorePunctuation": [],
            "synonyms": {}
        };

        let discoveredKeywords = [...new Set(discovered_keywords)];
        discoveredKeywords = discoveredKeywords.sort((a, b) => {
            return b.length - a.length;
        });

        const iFrameElem: any = document.getElementById('iframe_original_desc');
        if (iFrameElem != undefined) {
            const doc = iFrameElem.contentWindow.document;
            const markInstance = new Mark(doc);
            markInstance.unmark({
                done: function () {
                    markInstance.mark(discoveredKeywords, options);
                }
            });
        }
    }

    public highlightSubject(keywords_future_dates: string[], msg_subject: string, flag: boolean): string {
        if (!flag) {
            return msg_subject;
        }
        const options = {
            "separateWordSearch": false,
            "accuracy": "partially",
            "diacritics": true,
            "acrossElements": true,
            "caseSensitive": false,
            "ignoreJoiners": true,
            "exclude": [],
            "ignorePunctuation": [],
            "synonyms": {}
        };

        let keywordsFutureDates = [...new Set(keywords_future_dates)];
        keywordsFutureDates = keywordsFutureDates.sort((a, b) => {
            return b.length - a.length;
        });

        const msg_subject_elem = document.createElement("span");   // Create a <button> element
        msg_subject_elem.innerHTML = msg_subject;

        const doc = msg_subject_elem
        const markInstance = new Mark(doc);
        markInstance.unmark({
            done: function () {
                markInstance.mark(keywordsFutureDates, options);
            }
        });
        return doc.innerHTML;
    }


    public highlightKeywordsInTranslatedBody(discovered_keywords: string[], msg_body_translated: string, flag: boolean): string {
        try {
            if (!flag) {
                return msg_body_translated;
            }
            let discoveredKeywords = [...new Set(discovered_keywords)];
            discoveredKeywords = discoveredKeywords.sort((a, b) => {
                return b.length - a.length;
            });
            for (const key in discoveredKeywords) {
                if (Object.prototype.hasOwnProperty.call(discoveredKeywords, key)) {
                    const keyword = discoveredKeywords[key];
                    if (keyword != null || keyword != undefined) {
                        const sanitize_keyword = keyword.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
                        const regex = new RegExp("\\b" + sanitize_keyword + "\\b", "g");
                        msg_body_translated = msg_body_translated.replace(regex, "<span class='bg-yellow'>" + keyword + "</span>")
                    }
                }
            }
        } catch (error) {
            console.log("ERROR", error.message);
        }
        return msg_body_translated;
    }



    public writeToFrameBody(htmlBody: any): void {
        const iFrameElem: any = document.getElementById('iframe_original_desc');
        if (iFrameElem != undefined) {
            $("#iframe_original_desc").contents().scrollTop(0);
            const doc = iFrameElem.contentWindow.document;
            doc.open();
            doc.write(htmlBody ? htmlBody : BLANK);
            doc.close();
            this.addTargetInAnchorsIfNotExistInIframe(iFrameElem);
            setTimeout(() => {
                const contentDocument = iFrameElem?.contentDocument;
                contentDocument.documentElement.scrollTop = 0;
            }, 0);
        }
    }


    public writetoIframe(feedDetail: IFeed): void {
        const msg_body_HTML_COPY = (' ' + feedDetail.msg_body_HTML).slice(1);
        const iFrameElem: any = document.getElementById('iframe_original_desc');
        if (iFrameElem != undefined) {
            $("#iframe_original_desc").contents().scrollTop(0);
            const doc = iFrameElem.contentWindow.document;
            doc.open();
            if (msg_body_HTML_COPY != 'undefined') {
                doc.write(msg_body_HTML_COPY ? msg_body_HTML_COPY : BLANK);
            } else {
                doc.write(BLANK);
            }

            doc.close();
            this.addTargetInAnchorsIfNotExistInIframe(iFrameElem);
            setTimeout(() => {
                const contentDocument = iFrameElem?.contentDocument;
                contentDocument.documentElement.scrollTop = 0;
            }, 0);
        }
    }

    /**
 * add target in anchors if not exist in iframe
 * @param iframe
 */
    private addTargetInAnchorsIfNotExistInIframe(iframe: any): void {
        if (iframe != null && iframe != undefined) {
            const anchors = iframe.contentWindow.document.getElementsByTagName('a');
            for (const i in anchors) {
                if (typeof anchors[i] === "object")
                    anchors[i].setAttribute("target", "_blank");
            }
        }
    }


}
