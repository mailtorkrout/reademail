/* eslint-disable @typescript-eslint/no-explicit-any */
import { of } from 'rxjs';
import { BLANK, DEFAULT_PAGE } from '../app.config';
import { IFeed } from '../models/feedModel';
import { FilterModel } from '../models/filterModel';
import { FilterService } from './feeds.filter.service';
import { FeedsService } from './feeds.service';
import { GlobalService } from './global.service';

describe('FilterService', () => {

    let feedsService: FeedsService;
    let httpClientSpy: { get: jasmine.Spy };
    let mockGlobalService: GlobalService;
    let mockFeedDetail: IFeed;
    let mockfilterObject: FilterModel;
    let mockfilterObjectwithMailboxZero: FilterModel;
    let filterService: FilterService;

    beforeEach(function () {
        httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post']);
        mockGlobalService = new GlobalService();
        feedsService = new FeedsService(httpClientSpy as any, mockGlobalService);

        filterService = new FilterService(feedsService, mockGlobalService);

        mockFeedDetail = {
            "batch_id": 1038,
            "discovered_dates": ["year"],
            "discovered_keywords": ["year"],
            "future_date_score": 1.00,
            "is_actual_html": true,
            "keyword_score": 1.52,
            "listed_domain": false,
            "msg_body": "test message body",
            "msg_body_HTML": "Test message HTML <a>Hello</a> year",
            "msg_body_translated": BLANK,
            "msg_from": "donotreply@wordpress.com",
            "msg_id": "<59947194.47732.0@wordpress.com>",
            "msg_received_dt": new Date(),
            "msg_subject": "[New post] Critical Missing Person Richard Green",
            "msg_to": [
                "aptexas@ap.org"
            ],
            "originator_id": 1,
            "relevancy": true,
            "reviewed_date": new Date(),
            "reviewed_decision_id": 30,
            "reviewedby_user_id": 1,
            "search_score": 3.21,
            "shared_mailbox_id": 504,
            "total_score": 4.21,
            "uuid": "71734d75-e87b-11eb-a02c-33d8c57487e9",
            "reviewed_by_username": BLANK,
            "review_comments":BLANK,
            "msg_language": "en"
        };

        mockfilterObject = {
            "relevancy": "1",
            "start_date": "",
            "end_date": "",
            "originator": "1",
            "page": "1",
            "originator_id": "",
            "reviewed_decision_id": "",
            "shared_mailbox_id": ""
        }
        mockfilterObjectwithMailboxZero = {
            "relevancy": "1",
            "start_date": "",
            "end_date": "",
            "originator": "1",
            "page": "2",
            "originator_id": "",
            "reviewed_decision_id": "",
            "shared_mailbox_id": "0"
        }
    });

    it('get and set page no', () => {
        filterService.setSelectedPage(1);
        const page = filterService.getSelectedPage();
        expect(page).toBe(DEFAULT_PAGE);
    });

    it('get and set page as subject and observable', () => {
        filterService.sendSelectedPageBSubject(1);
        filterService.getSelectedPageAsObservable().subscribe(
            data => {
                expect(data).toBe(DEFAULT_PAGE);
            }
        )
    });

    it('send and get selected originator id as observable', () => {
        filterService.sendsSlectedOriginatorId("1");
        filterService.getSlectedOriginatorId().subscribe(
            data => {
                expect(data).toBe("1");
            }
        );
        expect(filterService).toBeTruthy();
    });

    it('scroll to top feed list test', () => {
        filterService.scrollToTopFeedList();
        expect(filterService).toBeTruthy();
    });

    it('clear Feeds Filter Obj', () => {
        spyOn(filterService, 'clearFeedsFilterObj').and.callThrough();
        filterService.clearFeedsFilterObj();
        expect(filterService).toBeTruthy();
    });

    it('set feeds Filter Obj test', () => {
        spyOn(filterService, 'setFeedsFilterObj').and.callThrough();

        spyOn(feedsService, 'fetchAllFeedsWithFilter').and.returnValues(
            of({
                "mails": [
                    mockFeedDetail
                ],
                "max_records": 10,
                "total_pages": 1,
                "total_records": 1
            })
        )
        filterService.setFeedsFilterObj(mockfilterObject);
        const filterModel = filterService.getFeedsFilterObj();
        expect(filterModel).toEqual(mockfilterObject);
        expect(filterService).toBeTruthy();
    });

    it('set feeds Filter Obj test with ', () => {
        spyOn(filterService, 'setFeedsFilterObj').and.callThrough();

        spyOn(feedsService, 'fetchAllFeedsWithFilter').and.returnValues(
            of({
                "mails": [
                    mockFeedDetail,
                    mockFeedDetail
                ],
                "max_records": 10,
                "total_pages": 2,
                "total_records": 1
            })
        );
        spyOn(feedsService, 'getTotalPages').and.returnValue(2);
        filterService.setFeedsFilterObj(mockfilterObjectwithMailboxZero);
        const filterModel = filterService.getFeedsFilterObj();
        expect(filterModel).toEqual(mockfilterObjectwithMailboxZero);
        expect(filterService).toBeTruthy();
    });

    it('add Target In Anchors If Not Exist In Iframe', () => {
        spyOn(filterService, 'addTargetInAnchorsIfNotExistInIframe').and.callThrough();
        filterService.addTargetInAnchorsIfNotExistInIframe(null);
        filterService.addTargetInAnchorsIfNotExistInIframe(undefined);

        const iframe = document.createElement('iframe');
        const html = '<body>' + mockFeedDetail.msg_body_HTML + '</body>';
        document.body.appendChild(iframe);
        iframe.id = "iframe_original_desc";
        iframe?.contentWindow?.document.open();
        iframe?.contentWindow?.document.write(html);
        iframe?.contentWindow?.document.close();
        filterService.addTargetInAnchorsIfNotExistInIframe(iframe);
    });


});
