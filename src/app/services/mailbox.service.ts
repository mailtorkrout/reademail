import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { MailBoxListModel } from '../models/MailBoxModel';
import { UserDetailService } from '../services/userDetail.service';
@Injectable({
  providedIn: 'root',
})
export class MailboxService {
  constructor(
    private _userDetailService: UserDetailService,
    private _http: HttpClient
  ) {}
  private mailBoxList: MailBoxListModel = {} as MailBoxListModel;

  fetchAllMailBoxesWrtToUser(
    urlQueryStr: string
  ): Observable<MailBoxListModel> {
    const data: MailBoxListModel = {
      mailboxes: [
        {
          mailbox_id: 10,
          mailbox_name: 'Hello',
        },
        {
          mailbox_id: 20,
          mailbox_name: 'Hi',
        },
      ],
      user_id: "1",
    };
    return of(data);
  }

  getMailBoxList(): MailBoxListModel {
    return this.mailBoxList;
  }

  setMailBoxList(mailBoxList: MailBoxListModel): void {
    this.mailBoxList = mailBoxList;
  }
}
