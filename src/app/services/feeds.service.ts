/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { Injectable } from '@angular/core';
import { IFeed } from '../models/feedModel';
import { FeedWithPaginationModel } from '../models/FeedWithPaginationModel';
import { FeedsCountModel } from '../models/FeedsCountModel';
import { ReviewSubmitModel } from '../models/ReviewSubmitModel';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { GlobalService } from './global.service';
import { BLANK } from '../app.config';

@Injectable({
  providedIn: 'root',
})
export class FeedsService {
  private feeds: IFeed[] = [];

  private feedsSubject = new Subject<IFeed[]>();

  private feedDetailSubject = new Subject<IFeed>();

  constructor(
    private http: HttpClient,
    private _globalService: GlobalService
  ) {}

  fetchAllFeedCount(
    mailBoxId: number,
    start_date?: string,
    end_date?: string
  ): Observable<FeedsCountModel> {
    const data = {
      acccepted: 20,
      irrelevent: 1,
      pending: 5,
      rejected: 2,
      reviewed: 10,
      relevent: 50,
    } as FeedsCountModel;

    return of(data);
  }

  sendFeedDetail(feedDetail: IFeed): void {
    this.feedDetailSubject.next(feedDetail);
  }

  getFeedDetail(): Observable<IFeed> {
    return this.feedDetailSubject.asObservable();
  }

  sendFeeds(): void {
    this.feedsSubject.next(this.feeds);
  }

  removeFeedsByUid(uuid: string): void {
    const updatedFeeds = this.feeds.filter((feed) => {
      return feed.uuid !== uuid;
    });
    this.setFeeds(updatedFeeds);
    this.feedsSubject.next(updatedFeeds);
  }

  fetchAllFeedsWithFilter(
    urlQueryStr: string
  ): Observable<FeedWithPaginationModel> {
    const data: FeedWithPaginationModel = {
      mails: [
        {
          batch_id: 1376,
          discovered_dates: ['year'],
          discovered_keywords: ['year'],
          future_date_score: 1.0,
          is_actual_html: true,
          keyword_score: 0.0,
          listed_domain: false,
          msg_body: 'Hello msg_body',
          msg_body_HTML: 'Hello msg_body_HTML year',
          msg_body_translated: '',
          msg_from: 'sadashiv.borkar.gmail@.com',
          msg_id:
            '\r\n <AS8PR01MB7656296C11A7083D89D86E03A71B9@AS8PR01MB7656.eurprd01.prod.exchangelabs.com>',
          msg_received_dt: new Date(),
          msg_subject: 'Hello msg_subject',
          msg_to: ['sadashiv.borkar.gmail@.com'],
          originator_id: 1,
          relevancy: true,
          reviewed_date: new Date(),
          reviewed_decision_id: 30,
          reviewedby_user_id: 15,
          search_score: 0.0,
          shared_mailbox_id: 503,
          total_score: 1.0,
          uuid: 'fa9af88e-e492-11eb-a44a-afa41d49256a',
          reviewed_by_username: 'Ratikanta Rout',
          review_comments: '',
          msg_language: 'en',
        },
      ],
      max_records: 10,
      total_pages: 2,
      total_records: 10,
    };

    return of(data);
  }

  setFeeds(feeds: IFeed[]): void {
    this.feeds = feeds;
  }

  getFeeds(): IFeed[] {
    return this.feeds;
  }

  getFeedsWithFilter(): Observable<IFeed[]> {
    return this.feedsSubject.asObservable();
  }

  //Getting Feed count and feed count subject
  private feedsCount: FeedsCountModel = {} as FeedsCountModel;

  private feedsCountSubject = new Subject<FeedsCountModel>();

  getFeedsCount(): FeedsCountModel {
    return this.feedsCount;
  }
  setFeedCount(data: FeedsCountModel): void {
    this.feedsCount = data;
    this.feedsCountSubject.next(data);
  }

  getFeedsCountSubject(): Observable<FeedsCountModel> {
    return this.feedsCountSubject.asObservable();
  }

  submitReview(reviewData: ReviewSubmitModel): Observable<ReviewSubmitModel> {
    return of(reviewData);
  }

  private totalPages = 0;

  private totalRecords = 0;

  private totalRecordsBSubject = new BehaviorSubject<number>(0);

  getTotalPages(): number {
    return this.totalPages;
  }

  setTotalPages(totalPages: number): void {
    this.totalPages = totalPages;
  }
  getTotalRecords(): number {
    return this.totalRecords;
  }
  setTotalRecords(totalRecords: number): void {
    this.totalRecords = totalRecords;
  }
  getTotalRecordsBSubjectAsObservable(): Observable<number> {
    return this.totalRecordsBSubject.asObservable();
  }

  sendTotalRecordsBSubject(totalRecords: number): void {
    this.totalRecordsBSubject.next(totalRecords);
  }
}
