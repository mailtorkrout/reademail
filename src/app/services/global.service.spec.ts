/* eslint-disable @typescript-eslint/no-explicit-any */

import { BLANK } from "../app.config";
import { IFeed } from "../models/feedModel";
import { GlobalService } from "./global.service";

describe('GlobalService', () => {

    const date_str = "Wed, 21 Jul 2021 05:45:19 GMT";
    const mockFeedDetail: IFeed = {
        "batch_id": 1038,
        "discovered_dates": ["year"],
        "discovered_keywords": ["year"],
        "future_date_score": 1.00,
        "is_actual_html": true,
        "keyword_score": 1.52,
        "listed_domain": false,
        "msg_body": "test message body",
        "msg_body_HTML": "Test message HTML <a>Hello</a> year",
        "msg_body_translated": "Test message HTML <a>Hello</a> year",
        "msg_from": "donotreply@wordpress.com",
        "msg_id": "<59947194.47732.0@wordpress.com>",
        "msg_received_dt": new Date(),
        "msg_subject": "[New post] Critical Missing Person Richard Green",
        "msg_to": [
            "aptexas@ap.org"
        ],
        "originator_id": 1,
        "relevancy": true,
        "reviewed_date": new Date(),
        "reviewed_decision_id": 30,
        "reviewedby_user_id": 1,
        "search_score": 3.21,
        "shared_mailbox_id": 504,
        "total_score": 4.21,
        "uuid": "71734d75-e87b-11eb-a02c-33d8c57487e9",
        "reviewed_by_username": BLANK,
        "review_comments": BLANK,
        "msg_language": "en"
    };
    let globalService: GlobalService;

    beforeEach(function () {
        globalService = new GlobalService();
    });

    it('convertDate as give the result as yyy-mm-dd format', () => {
        const converted_date = globalService.convertDate(new Date(date_str));
        expect(converted_date).toEqual("2021-07-21");
    });

    it('sanitize the html data, replace unicodes to html unicodes', () => {
        const test_str = "r\\'s";
        const test_str_with_b1 = "b'r\\'s";
        const sanitized_string = globalService.sanitizeHtml(test_str, 1);
        const sanitized_string_with_b1 = globalService.sanitizeHtml(test_str_with_b1, 1);

        expect(sanitized_string).toEqual("r'");
        expect(sanitized_string_with_b1).toEqual("r'");

    });

    it('getQueryString convert a object to query sting', () => {

        const mockObj = {
            "id": 1,
            "name": undefined,
            "rollNo": "51"
        };
        const queryString = globalService.getQueryString(mockObj);
        expect(queryString).toEqual("?id=1&rollNo=51");

        expect(globalService.getQueryString({
            "id": undefined,
            "name": undefined,
            "rollNo": undefined
        })).toEqual(BLANK);
    });

    it('write to Iframe', () => {
        const iframe = document.createElement('iframe');
        const html = '<body></body>';
        document.body.appendChild(iframe);
        iframe.id = "iframe_original_desc";
        iframe?.contentWindow?.document.open();
        iframe?.contentWindow?.document.write(html);
        iframe?.contentWindow?.document.close();
        globalService.writetoIframe(mockFeedDetail);
        globalService.writeToFrameBody(mockFeedDetail);
        globalService.highlightEmailNDomain(mockFeedDetail.msg_from, true);
        globalService.highlightEmailNDomain(mockFeedDetail.msg_from, false);
        globalService.highlightKeywords(mockFeedDetail.discovered_keywords, mockFeedDetail.msg_body_HTML, true);
        globalService.highlightKeywords(mockFeedDetail.discovered_keywords, mockFeedDetail.msg_body_HTML, false);
        globalService.highlightKeywordsInTranslatedBody(mockFeedDetail.discovered_keywords, mockFeedDetail.msg_body_translated, false);
        globalService.highlightKeywordsInTranslatedBody(mockFeedDetail.discovered_keywords, mockFeedDetail.msg_body_translated, true);
        expect(globalService).toBeTruthy();
    });




});
