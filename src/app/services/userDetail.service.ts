import { Injectable } from '@angular/core';
import { BLANK } from '../app.config';
import { UserDetailCookieModel } from '../models/userDetailCookieModel';

@Injectable({
  providedIn: 'root',
})

/**
 * This service is used to get the userdetail from service
 */
export class UserDetailService {
  private getCookie(cname: string): string {
    const name = cname + '=';
    const decodedCookie = decodeURIComponent(document.cookie);
    const ca = decodedCookie.split(';');

    for (const element of ca) {
      let c = element;
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return BLANK;
  }

  private getUserDetailAsJson(data: string): UserDetailCookieModel {
    if (data !== BLANK) {
      let filteredData = data.replace(/\\054/g, ',');
      filteredData = filteredData.replace(/', '/g, '","');
      filteredData = filteredData.replace(/, '/g, ',"');
      filteredData = filteredData.replace(/': '/g, '":"');
      filteredData = filteredData.replace(/': /g, '":');
      filteredData = filteredData.replace(/"{'/g, '{"');
      filteredData = filteredData.replace(/'}"/g, '"}');
      filteredData = filteredData.replace(/,'/g, ',"');
      filteredData = filteredData.replace(/',/g, '",');
      return JSON.parse(filteredData);
    }
    return {} as UserDetailCookieModel;
  }

  public getUserDetailFromCookie(): UserDetailCookieModel {
    // const userDetail = this.getCookie('user');
    const userDetail = {
      aud: 'string',
      iss: 'string',
      iat: 'string',
      nbf: 'string',
      exp: 'string',
      aio: 'string',
      name: 'Ratikanta Rout',
      nonce: 'string',
      oid: 'string',
      preferred_username: 'ratikanta@gmail.com',
      uname: 'Ratikanta Rout',
      rh: 'string',
      sub: 'string',
      tid: 'string',
      uti: 'string',
      ver: 'string',
      uid: 10,
    };

    return userDetail;
  }

  public getTokenDetailFromCookie(name: string): string {
    let token = this.getCookie(name);
    if (name === 'token') {
      token = token.substring(2);
      token = token.slice(0, -1);
    }
    return token;
  }

  public getUserId(): number {
    const uid = localStorage?.getItem('uid');
    return parseInt(uid ? uid : '0');
  }

  public setUserId(uid: number): void {
    localStorage.setItem('uid', uid.toString());
  }
}
