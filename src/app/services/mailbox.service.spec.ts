/* eslint-disable @typescript-eslint/no-explicit-any */

import { MailboxService } from "./mailbox.service";
import { UserDetailService } from "./userDetail.service";
import { of } from 'rxjs';
import { MailBoxListModel } from "../models/MailBoxModel";
import { UserDetailCookieModel } from "../models/userDetailCookieModel";


describe('MailboxService', () => {

    let mailboxService: MailboxService;
    let mockUserDetailService: UserDetailService;
    let httpClientSpy: { get: jasmine.Spy };
    const mockMailBoxesList: MailBoxListModel = {
        "mailboxes":
            [{
                "mailbox_id": 503,
                "mailbox_name": "demo-fwd-planning@gmail.com"
            },
            {
                "mailbox_id": 504,
                "mailbox_name": "demo2@gmail.com"
            }
            ],
        "user_id": "107"
    };

    const mockuserDetailCookie: UserDetailCookieModel = {
        'aud': "815dc012-030c-4b1d-8956-43bcf2bf16b4",
        'iat': '1624942351',
        'nbf': '1624942351',
        'exp': '1624946251',
        'aio': "AUQAu/8TAAAAKp5eA/xxH50ZDi+IRh/ZLnw16LRnV+9oVoLN+dFa010g4OalHFy2iho25tcC62pK+hS9agNWHpM5DTFYfNxZ/A==",
        'iss': "https://login.microsoftonline.com/1f4beacd-b7aa-49b2-aaa1-b8525cb257e0/v2.0",
        'name': "Ratikanta Rout",
        'nonce': "433c1e1b2a3000f5d81f9d76025fd21055db12181485a096292269d2ae277b4c",
        'oid': "a2132444-ef5e-4546-80e6-3fe968c982c4",
        'preferred_username': "ratikanta.rout.gmail@.com",
        'rh': "0.AVQAzepLH6q3skmqobhSXLJX4BLAXYEMAx1LiVZDvPK_FrRUALk.",
        'sub': "m1jdHEd3_voCmScv9wH3W4aPNJx2Ibid-HaF3C4TbOM",
        'tid': "1f4beacd-b7aa-49b2-aaa1-b8525cb257e0",
        'uti': "IkBvuQXBrUCCfDre2AFrAA",
        'ver': "2.0",
        "uname": "",
        "uid": 107
    }

    beforeEach(function () {
        httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
        mockUserDetailService = new UserDetailService();
        spyOn(mockUserDetailService, 'getUserDetailFromCookie').and.returnValue(mockuserDetailCookie);
        mailboxService = new MailboxService(mockUserDetailService, httpClientSpy as any);
    });

    it('fetch all mail boxes wrt to user', (done: DoneFn) => {
        httpClientSpy.get.and.returnValue(of(mockMailBoxesList));

        mailboxService.fetchAllMailBoxesWrtToUser("?email = " + mockuserDetailCookie.preferred_username).subscribe(
            data => {
                expect(data).toEqual(mockMailBoxesList);
                done();
            },
            done.fail
        );
        expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
    });

    it('test get mailbox list and set mailboxlist', () => {
        mailboxService.setMailBoxList(mockMailBoxesList);
        const result = mailboxService.getMailBoxList();
        expect(result).toEqual(mockMailBoxesList);
    });

});
