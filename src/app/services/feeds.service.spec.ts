/* eslint-disable @typescript-eslint/no-explicit-any */
import { of } from 'rxjs';
import { BLANK } from '../app.config';
import { IFeed } from '../models/feedModel';
import { FeedsCountModel } from '../models/FeedsCountModel';
import { FeedWithPaginationModel } from '../models/FeedWithPaginationModel';
import { ReviewSubmitModel } from '../models/ReviewSubmitModel';
import { FeedsService } from './feeds.service';
import { GlobalService } from './global.service';

describe('FeedsService', () => {

    let feedsService: FeedsService;
    let httpClientSpy: { get: jasmine.Spy };
    let mockGlobalService: GlobalService;
    let mockfeedWithPagination: FeedWithPaginationModel;
    let mockFeedDetail: IFeed;
    let mockFeedCount: FeedsCountModel;
    let mockReviewSubmit: ReviewSubmitModel;

    beforeEach(function () {
        httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post']);
        mockGlobalService = new GlobalService();
        feedsService = new FeedsService(httpClientSpy as any, mockGlobalService);

        mockFeedDetail = {
            "batch_id": 1038,
            "discovered_dates": ["year"],
            "discovered_keywords": ["year"],
            "future_date_score": 1.00,
            "is_actual_html": true,
            "keyword_score": 1.52,
            "listed_domain": false,
            "msg_body": "test message body",
            "msg_body_HTML": "Test message HTML year",
            "msg_body_translated": BLANK,
            "msg_from": "donotreply@wordpress.com",
            "msg_id": "<59947194.47732.0@wordpress.com>",
            "msg_received_dt": new Date(),
            "msg_subject": "[New post] Critical Missing Person Richard Green",
            "msg_to": [
                "aptexas@ap.org"
            ],
            "originator_id": 1,
            "relevancy": true,
            "reviewed_date": new Date(),
            "reviewed_decision_id": 30,
            "reviewedby_user_id": 1,
            "search_score": 3.21,
            "shared_mailbox_id": 504,
            "total_score": 4.21,
            "uuid": "71734d75-e87b-11eb-a02c-33d8c57487e9",
            "reviewed_by_username": BLANK,
            "review_comments":BLANK,
            "msg_language": "en"
        };

        mockfeedWithPagination = {
            "mails": [mockFeedDetail],
            "max_records": 1,
            "total_pages": 1,
            "total_records": 1
        }

        mockReviewSubmit = {
            uuid: "71734d75-e87b-11eb-a02c-33d8c57487e9",
            reviewed_by: 10,
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            review_date: new Date(),
            reviewed_decision_id: "10",
            review_comments:""
        }

        mockFeedCount = {
            "acccepted": 3,
            "irrelevent": 795,
            "pending": 5048,
            "rejected": 3,
            "relevent": 4259,
            "reviewed": 6
        }
    });

    it('fetch all feeds with filter', (done: DoneFn) => {
        httpClientSpy.get.and.returnValue(of(mockfeedWithPagination));

        // spyOn(feedsService, "fetchAllFeedsWithFilter").and.returnValue(of(mockfeedWithPagination))
        feedsService.fetchAllFeedsWithFilter("?page=1").subscribe(
            data => {
                expect(data).toEqual(mockfeedWithPagination);
                done();
            },
        );
        expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
    });


    it('fetch all feed count', (done: DoneFn) => {
        httpClientSpy.get.and.returnValue(of(mockFeedCount));
        feedsService.fetchAllFeedCount(1, (new Date()).toDateString(), (new Date()).toDateString()).subscribe(
            data => {
                expect(data).toEqual(mockFeedCount);
                done();
            },
            done.fail);
        expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');


        feedsService.fetchAllFeedCount(0, (new Date()).toDateString(), (new Date()).toDateString()).subscribe(
            result => {
                expect(result).toEqual(mockFeedCount);
                done();
            },
            done.fail);
        expect(httpClientSpy.get.calls.count()).toBe(2, 'two call');
    });

    it('send and get feed detail', () => {
        let response = {} as IFeed;
        feedsService.getFeedDetail().subscribe(
            data => response = data
        );
        feedsService.sendFeedDetail(mockFeedDetail);
        expect(response).toEqual(mockFeedDetail);
    });

    it('send feeds', () => {
        let response: IFeed[] = [];
        spyOn(feedsService, "fetchAllFeedsWithFilter").and.returnValue(of(mockfeedWithPagination))
        feedsService.fetchAllFeedsWithFilter("?page=1").subscribe(data => {
            response = data.mails;
            feedsService.sendFeeds();
        });
        feedsService.getFeedsWithFilter().subscribe(
            data => response = data
        );
        expect(response).toEqual([mockFeedDetail]);
    });

    it('remove feeds by uid', () => {
        let response: IFeed[] = [];
        const uid = "71734d75-e87b-11eb-a02c-33d8c57487e9";
        spyOn(feedsService, "fetchAllFeedsWithFilter").and.returnValue(of(mockfeedWithPagination))
        feedsService.fetchAllFeedsWithFilter("?page=1").subscribe(data => {
            response = data.mails;
            feedsService.sendFeeds();
        });
        feedsService.getFeedsWithFilter().subscribe(
            data => response = data
        );
        feedsService.removeFeedsByUid(uid);
        expect(response).toEqual([]);

    });

    it('remove feeds by uid', () => {
        let response: IFeed[] = [];
        const uid = "71734d75-e87b-11eb-a02c-33d8c57487e9";
        spyOn(feedsService, "fetchAllFeedsWithFilter").and.returnValue(of(mockfeedWithPagination))
        feedsService.fetchAllFeedsWithFilter("?page=1").subscribe(data => {
            response = data.mails;
            feedsService.sendFeeds();
        });
        feedsService.getFeedsWithFilter().subscribe(
            data => response = data
        );
        feedsService.removeFeedsByUid(uid);
        expect(response).toEqual([]);

    });

    it('get and set feed count', () => {
        let response = {} as FeedsCountModel;
        spyOn(feedsService, "fetchAllFeedCount").and.returnValue(of(mockFeedCount))
        feedsService.fetchAllFeedCount(1, (new Date()).toDateString(), (new Date()).toDateString()).subscribe(data => {
            response = data;
        });
        feedsService.setFeedCount(response);
        const feedCount = feedsService.getFeedsCount();
        expect(JSON.stringify(response)).toEqual(JSON.stringify(feedCount));
    });

    it('get and set feed count', () => {
        let response = {} as FeedsCountModel;
        spyOn(feedsService, "fetchAllFeedCount").and.returnValue(of(mockFeedCount))
        feedsService.fetchAllFeedCount(1, (new Date()).toDateString(), (new Date()).toDateString()).subscribe(data => {
            response = data;
        });
        let feedCount: FeedsCountModel = {} as FeedsCountModel;
        feedsService.getFeedsCountSubject().subscribe(
            data => feedCount = data
        );
        feedsService.setFeedCount(response);
        expect(JSON.stringify(response)).toEqual(JSON.stringify(feedCount));
    });

    it('submit review', () => {
        let response: any;
        spyOn(feedsService, "submitReview").and.returnValue(of(mockReviewSubmit))
        feedsService.submitReview(mockReviewSubmit).subscribe(data => {
            response = data;
        });
        expect(JSON.stringify(response)).toEqual(JSON.stringify(mockReviewSubmit));
    });

    it('check count of get and set total pages', () => {
        spyOn(feedsService, "fetchAllFeedsWithFilter").and.returnValue(of(mockfeedWithPagination))
        feedsService.fetchAllFeedsWithFilter("?page=1").subscribe(data => {
            feedsService.setTotalPages(data.total_pages);
        });
        const total_pages = feedsService.getTotalPages();
        expect(mockfeedWithPagination.total_pages).toEqual(total_pages);
    });


    it('check count of get and set total Records', () => {
        spyOn(feedsService, "fetchAllFeedsWithFilter").and.returnValue(of(mockfeedWithPagination))
        feedsService.fetchAllFeedsWithFilter("?page=1").subscribe(data => {
            feedsService.setTotalRecords(data.total_pages);
        });
        const total_records = feedsService.getTotalRecords();
        expect(mockfeedWithPagination.total_pages).toEqual(total_records);
    });

    it('check count of get and set total Records', () => {
        spyOn(feedsService, "fetchAllFeedsWithFilter").and.returnValue(of(mockfeedWithPagination))
        feedsService.fetchAllFeedsWithFilter("?page=1").subscribe(data => {
            feedsService.setTotalRecords(data.total_pages);
        });
        const total_records = feedsService.getTotalRecords();
        expect(mockfeedWithPagination.total_pages).toEqual(total_records);
    });

    it('check count of send and get total Records as observable', () => {
        spyOn(feedsService, "fetchAllFeedsWithFilter").and.returnValue(of(mockfeedWithPagination))
        feedsService.fetchAllFeedsWithFilter("?page=1").subscribe(data => {
            feedsService.sendTotalRecordsBSubject(data.total_pages);
        });
        let total_records = 0;
        feedsService.getTotalRecordsBSubjectAsObservable().subscribe(
            data => total_records = data
        );
        expect(mockfeedWithPagination.total_pages).toEqual(total_records);
    });



});
