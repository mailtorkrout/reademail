/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { Injectable } from '@angular/core';
import { FeedWithPaginationModel } from '../models/FeedWithPaginationModel'
import { FilterModel } from '../models/filterModel'
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { FeedsService } from './feeds.service';
import { GlobalService } from './global.service';
import { DEFAULT_PAGE, ONE_NUMBER, ZERO_STRING, BLANK } from './../app.config';
import { IFeed } from '../models/feedModel';

@Injectable({
    providedIn: 'root'
})

/**
 * This Service will help all filter related activities.
 */
export class FilterService {

    private selectedOriginator = new Subject<string>();

    private selectedPage: string = DEFAULT_PAGE;

    /**
     * This will get the selected page.
     * @returns selectedPage
     */
    getSelectedPage(): string {
        return this.selectedPage;
    }
    /**
     * this will set the selected page in the observable.
     */
    setSelectedPage(selectedPage: number): void {
        this.selectedPage = selectedPage.toString();
    }

    private selectedPageBSubject: BehaviorSubject<string> = new BehaviorSubject<string>(DEFAULT_PAGE);

    /**
     * Send selected page to behaviour subject.
     * @param pageNumber page number
     */
    public sendSelectedPageBSubject(pageNumber: number): void {
        this.setSelectedPage(pageNumber);
        this.selectedPageBSubject.next(pageNumber.toString());
    }

    /**
     * get selected page as observable
     * @returns selectedPage
     */
    public getSelectedPageAsObservable(): Observable<string> {
        return this.selectedPageBSubject.asObservable();
    }
    /**
     * sends slected originator id
     * @param selectedOriginator_id 
     */
    sendsSlectedOriginatorId(selectedOriginator_id: string): void {
        this.selectedOriginator.next(selectedOriginator_id);
    }
    /**
     * get slected originator id
     * @returns slectedOriginatorId
     */
    getSlectedOriginatorId(): Observable<string> {
        return this.selectedOriginator.asObservable();
    }

    private feedsFilterObj: FilterModel = { page: DEFAULT_PAGE } as FilterModel;

    /**
     * Constructor for feeds filter service.
     * @param _feedsService 
     * @param _globalService 
     */
    constructor(private _feedsService: FeedsService,
        private _globalService: GlobalService) {
    }

    /**
     * this will auto scroll to top of feedlist
     */
    scrollToTopFeedList(): void {
        const feedList = document.getElementById("feedList");
        if (feedList)
            feedList.scrollTop = 0;
    }

    /**
     * this will use for clear feeds filter object
     */
    clearFeedsFilterObj(): void {
        this.scrollToTopFeedList();
        this.feedsFilterObj = {
            page: DEFAULT_PAGE,
            shared_mailbox_id: this.feedsFilterObj.shared_mailbox_id,
            start_date: this.feedsFilterObj.start_date,
            end_date: this.feedsFilterObj.end_date
        } as FilterModel;
    }

    /**
     * append feeds if not defaultpage=1
     * @param mails 
     */
    private appendFeedsIfNotDefaultPage(mails: IFeed[]): void {
        const oldFeeds = this._feedsService.getFeeds();
        const newFeeds = mails;
        newFeeds.forEach((elem) => {
            oldFeeds.push(elem);
        });
        this._feedsService.setFeeds(oldFeeds);
    }

    /**
     * set feeds if default page
     * @param data 
     */
    private setFeedsIfDefaultPage(data: FeedWithPaginationModel): void {
        this.setSelectedPage(parseInt(DEFAULT_PAGE));
        this._feedsService.sendTotalRecordsBSubject(data.total_records);
        this._feedsService.setTotalPages(data.total_pages);
        this._feedsService.setFeeds(data.mails);
    }

    /**
     * this function will help to set feeds filter object and to get the
     * feedlist
     * @param filterObj 
     */
    setFeedsFilterObj(filterObj: FilterModel): void {
        if (filterObj.shared_mailbox_id == ZERO_STRING)
            filterObj.shared_mailbox_id = BLANK;
        if (filterObj.page === DEFAULT_PAGE || parseInt(filterObj.page) <= this._feedsService.getTotalPages()) {
            this.feedsFilterObj = filterObj;
            this._feedsService.fetchAllFeedsWithFilter(this._globalService.getQueryString(this.feedsFilterObj))
                .subscribe((data: FeedWithPaginationModel) => {
                    data.mails.forEach((elem, index) => {
                        if (elem.is_actual_html) {
                            if (data.mails[index].originator_id === ONE_NUMBER)
                                data.mails[index].msg_body_HTML = this._globalService.sanitizeHtml(elem.msg_body_HTML, 1);
                            data.mails[index].msg_body_HTML = this._globalService.sanitizeHtml(elem.msg_body_HTML, 0);
                        }
                    });
                    filterObj.page === DEFAULT_PAGE ? this.setFeedsIfDefaultPage(data) : this.appendFeedsIfNotDefaultPage(data.mails);
                    this._feedsService.sendFeeds();
                });
        }
    }

    /**
     * get feeds filter object
     * @returns feedsFilterObj
     */
    getFeedsFilterObj(): FilterModel {
        return this.feedsFilterObj;
    }

    /**
     * add target in anchors if not exist in iframe
     * @param iframe 
     */
    addTargetInAnchorsIfNotExistInIframe(iframe: any): void {
        if (iframe != null && iframe != undefined) {
            const anchors = iframe.contentWindow.document.getElementsByTagName('a');
            for (const i in anchors) {
                if (typeof anchors[i] === "object")
                    anchors[i].setAttribute("target", "_blank");
            }
        }
    }
}
