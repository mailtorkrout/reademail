/* eslint-disable @typescript-eslint/no-explicit-any */

import { TestBed } from "@angular/core/testing";
import { UserDetailCookieModel } from "../models/userDetailCookieModel";
import { UserDetailService } from "./userDetail.service";

describe('UserDetailService', () => {

    let userDetailService: UserDetailService;
    const mockuserDetailCookie: UserDetailCookieModel = {
        'aud': "815dc012-030c-4b1d-8956-43bcf2bf16b4",
        'iat': '1624942351',
        'nbf': '1624942351',
        'exp': '1624946251',
        'aio': "AUQAu/8TAAAAKp5eA/xxH50ZDi+IRh/ZLnw16LRnV+9oVoLN+dFa010g4OalHFy2iho25tcC62pK+hS9agNWHpM5DTFYfNxZ/A==",
        'iss': "https://login.microsoftonline.com/1f4beacd-b7aa-49b2-aaa1-b8525cb257e0/v2.0",
        'name': "Ratikanta Rout",
        'nonce': "433c1e1b2a3000f5d81f9d76025fd21055db12181485a096292269d2ae277b4c",
        'oid': "a2132444-ef5e-4546-80e6-3fe968c982c4",
        'preferred_username': "ratikanta.rout.gmail@.com",
        'rh': "0.AVQAzepLH6q3skmqobhSXLJX4BLAXYEMAx1LiVZDvPK_FrRUALk.",
        'sub': "m1jdHEd3_voCmScv9wH3W4aPNJx2Ibid-HaF3C4TbOM",
        'tid': "1f4beacd-b7aa-49b2-aaa1-b8525cb257e0",
        'uti': "IkBvuQXBrUCCfDre2AFrAA",
        'ver': "2.0",
        "uname": "",
        "uid": 107
    }

    function setCookie() {
        document.cookie = "user=\"{'aud': '815dc012-030c-4b1d-8956-43bcf2bf16b4'\\054 'iss': 'https://login.microsoftonline.com/1f4beacd-b7aa-49b2-aaa1-b8525cb257e0/v2.0'\\054 'iat': 1624942351\\054 'nbf': 1624942351\\054 'exp': 1624946251\\054 'aio': 'AUQAu/8TAAAAKp5eA/xxH50ZDi+IRh/ZLnw16LRnV+9oVoLN+dFa010g4OalHFy2iho25tcC62pK+hS9agNWHpM5DTFYfNxZ/A=='\\054 'name': 'Ratikanta Rout'\\054 'nonce': '433c1e1b2a3000f5d81f9d76025fd21055db12181485a096292269d2ae277b4c'\\054 'oid': 'a2132444-ef5e-4546-80e6-3fe968c982c4'\\054 'preferred_username': 'ratikanta.rout.gmail@.com'\\054 'rh': '0.AVQAzepLH6q3skmqobhSXLJX4BLAXYEMAx1LiVZDvPK_FrRUALk.'\\054 'sub': 'm1jdHEd3_voCmScv9wH3W4aPNJx2Ibid-HaF3C4TbOM'\\054 'tid': '1f4beacd-b7aa-49b2-aaa1-b8525cb257e0'\\054 'uti': 'IkBvuQXBrUCCfDre2AFrAA'\\054 'ver': '2.0'}\"";
        document.cookie = "token=TEABCI";
    }

    beforeEach(function () {
        userDetailService = new UserDetailService();
        userDetailService = TestBed.get(UserDetailService);
    });

    afterEach(function () {
        setCookie();
    })

    it('get cookie as some name', () => {
        const name = "ratikanta";
        spyOn(UserDetailService.prototype as any, "getCookie").and.returnValue(name);
        const data = userDetailService['getCookie']('user');
        expect(data).toBe(name);
    });

    it('get cookie as BLANK', () => {
        const name = "";
        spyOn(UserDetailService.prototype as any, "getCookie").and.returnValue(name);
        const data = userDetailService['getCookie']('user');
        expect(data).toBe(name);
    });

    it('get User Detail As Json', () => {
        const sampleObj = {} as UserDetailCookieModel;
        spyOn(UserDetailService.prototype as any, "getUserDetailAsJson").and.callThrough();
        const userDetail = userDetailService['getUserDetailAsJson']("{}");
        expect(userDetail).toEqual(sampleObj);
    });

    it('get user detail from cookie', () => {
        const name = "";
        spyOn(UserDetailService.prototype as any, "getCookie").and.returnValue(name);
        spyOn(UserDetailService.prototype as any, "getUserDetailAsJson").and.returnValue(mockuserDetailCookie);
        const userDetail = userDetailService.getUserDetailFromCookie();
        expect(userDetail.preferred_username).toEqual(mockuserDetailCookie.preferred_username);

    });

    xit('get token detail from cookie', () => {
        spyOn(userDetailService, "getTokenDetailFromCookie").and.callThrough();
        const token = userDetailService.getTokenDetailFromCookie("token");
        expect(token).toEqual("ABC");
    });

    it('get and set user id', () => {
        userDetailService.setUserId(10);
        const userId = userDetailService.getUserId();
        expect(userId).toEqual(10);
    });

});