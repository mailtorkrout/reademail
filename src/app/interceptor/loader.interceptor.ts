/* eslint-disable @typescript-eslint/no-explicit-any */
import { Injectable } from "@angular/core";
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { finalize } from "rxjs/operators";
import * as $ from 'jquery'
import { UserDetailService } from './../services/userDetail.service';
@Injectable()
export class LoaderInterceptor implements HttpInterceptor {

  constructor(private _userDetailService: UserDetailService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const token = this._userDetailService.getTokenDetailFromCookie("token");

    const csrfToken = this._userDetailService.getTokenDetailFromCookie("CSRF-TOKEN");

    console.log("csrfToken", csrfToken);

    const modifiedReq = req.clone({

      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'charset': 'utf8mb4_unicode_ci',
        'Authorization': "Bearer " + token,
        "X-CSRFToken": csrfToken,
        "Access-Control-Allow-Methods": "DELETE, POST, GET, OPTIONS",
        "Access-Control-Allow-Headers": "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With"

      }),
      params: req.params.set(
        "user_id", this._userDetailService.getUserId()
      )

    });

    $("#blocker").css("display", "flex");
    return next.handle(modifiedReq).pipe(
      finalize(() => {
        $("#blocker").css("display", "none");
      })
    );
  }
}